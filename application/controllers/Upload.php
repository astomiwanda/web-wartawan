<?php 
 
class Upload extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		  $this->load->helper(array('form', 'url'));
	}
 
	public function index(){
		$this->load->view('v_upload', array('error' => ' ' ));
	}
 
	public function aksi_upload(){
		$config['upload_path']          = './assets/uploads/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 10000;
		$config['overwrite'] 			= TRUE;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('berkas')){
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('v_upload', $error);
		}else{
			$data = array('upload_data' => $this->upload->data());
			$this->load->view('v_upload_sukses', $data);
		}

		// $arrayName = array('user_id' => 1,
		// 'judul' => 'Jajal',
		// 'isi' => 'jajal isine',
		// 'lokasi' => 'jakarta',
		// 'tema' => 'politik',
		// 'kategori_id' => 1,
		// 'waktu' => '08:30:00',
		// 'tanggal' => '2019-02-20' );

		// $dataInfo = array();
		// $files = $_FILES;
		// $cpt = count($_FILES['berkas']['name']);
		// for($i=0; $i<$cpt; $i++)
		// {           
		// 	$_FILES['berkas']['name']= $files['berkas']['name'][$i];
		// 	$_FILES['berkas']['type']= $files['berkas']['type'][$i];
		// 	$_FILES['berkas']['tmp_name']= $files['berkas']['tmp_name'][$i];
		// 	$_FILES['berkas']['error']= $files['berkas']['error'][$i];
		// 	$_FILES['berkas']['size']= $files['berkas']['size'][$i];    

		// 	$this->upload->do_upload('berkas');
		// 	$dataInfo[] = $this->upload->data();
		// 	$this->db->insert('berita', array('img' => $_FILES['berkas']['name'] ));
		// 	$this->db->insert_id();
		// 	print_r($dataInfo[$i]);
		// 	echo "<br>";
		// }
	}
	
}