<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Politik extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($page='')
	{
		if ($page == '') {
			$page = 0;
		}
		
		$query = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		ORDER BY `id_berita` DESC";
		$data_berita = $this->M_Berita->query($query);
		$i = 0;
		foreach ($data_berita as $key => $value) {
			$time_ago[$i++]= $this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		$this->load->database();
		$jumlah_data = $this->M_Berita->jumlah_data(3);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'Terkini/index';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
        $from = $this->uri->segment(3,0);
        $this->pagination->initialize($config);	
        $data['pagination'] = $this->pagination->create_links();
		$data['data_berita_limit'] = $this->M_Berita->getBerita($config['per_page'],$from,3);
		$data['data_berita'] = $data_berita;
		$data['page'] = $page;
		$data['jumlah_data'] = $jumlah_data;
		$this->load->view('politik',$data);
	}
}
