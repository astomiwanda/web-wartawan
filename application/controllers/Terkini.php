<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terkini extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index_b()
	{
		$query = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		WHERE `berita`.`kategori_id` = 1
		ORDER BY `id_berita` DESC";
		$data_berita = $this->M_Berita->query($query);
		$i = 0;
		foreach ($data_berita as $key => $value) {
			$time_ago[$i++]= $this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		$arrayName = array('data_berita' => $data_berita );
		$this->load->view('terkini');
	}

	public function index($page='')
	{
		if ($page == '') {
			$page = 0;
		}
		
		$query = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		ORDER BY `id_berita` DESC";
		$data_berita = $this->M_Berita->query($query);
		$i = 0;
		foreach ($data_berita as $key => $value) {
			$time_ago[$i++]= $this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		$this->load->database();
		$jumlah_data = $this->M_Berita->jumlah_data(1);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'Terkini/index';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
        $from = $this->uri->segment(3,0);
        $this->pagination->initialize($config);	
        $data['pagination'] = $this->pagination->create_links();
		$data['data_berita_limit'] = $this->M_Berita->getBerita($config['per_page'],$from,1);
		$data['data_berita'] = $data_berita;
		$data['page'] = $page;
		$data['jumlah_data'] = $jumlah_data;
		$this->load->view('terkini',$data);
	}

	public static function time_elapsed_string($datetime, $full = false) {
		$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);
	
		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;
	
		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}
	
		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}
