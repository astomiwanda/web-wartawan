<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$query = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		ORDER BY `id_berita` DESC";
		$data_berita = $this->M_Berita->query($query);
		$i = 0;
		foreach ($data_berita as $key => $value) {
			$time_ago[$i++]= $this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		$query_terkini = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		WHERE `berita`.`kategori_id` = 1
		ORDER BY `id_berita` DESC";
		$berita_terkini = $this->M_Berita->query($query_terkini);
		$i = 0;
		foreach ($berita_terkini as $key => $value) {
			$time_ago_terkini[$i++]=$this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		$query_pemkot = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		WHERE `berita`.`kategori_id` = 2
		ORDER BY `id_berita` DESC";
		$berita_pemkot = $this->M_Berita->query($query_pemkot);
		$i = 0;
		foreach ($berita_pemkot as $key => $value) {
			$time_ago_pemkot[$i++]=$this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		$query_politik = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		WHERE `berita`.`kategori_id` = 3
		ORDER BY `id_berita` DESC";
		$berita_politik = $this->M_Berita->query($query_politik);
		$i = 0;
		foreach ($berita_politik as $key => $value) {
			$time_ago_politik[$i++]=$this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		$query_pendidikan = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		WHERE `berita`.`kategori_id` = 4
		ORDER BY `id_berita` DESC";
		$berita_pendidikan = $this->M_Berita->query($query_pendidikan);
		$i = 0;
		foreach ($berita_pendidikan as $key => $value) {
			$time_ago_pendidikan[$i++]=$this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		$query_nasional = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		WHERE `berita`.`kategori_id` = 5
		ORDER BY `id_berita` DESC";
		$berita_nasional = $this->M_Berita->query($query_nasional);
		$i = 0;
		foreach ($berita_nasional as $key => $value) {
			$time_ago_nasional[$i++]=$this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		$query_inspirasi = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		WHERE `berita`.`kategori_id` = 6
		ORDER BY `id_berita` DESC";
		$berita_inspirasi = $this->M_Berita->query($query_inspirasi);
		$i = 0;
		foreach ($berita_inspirasi as $key => $value) {
			$time_ago_inspirasi[$i++]=$this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		$query_peristiwa = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		WHERE `berita`.`kategori_id` = 7
		ORDER BY `id_berita` DESC";
		$berita_peristiwa = $this->M_Berita->query($query_peristiwa);
		$i = 0;
		foreach ($berita_peristiwa as $key => $value) {
			$time_ago_peristiwa[$i++]=$this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		$query_umkm = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		WHERE `berita`.`kategori_id` = 8
		ORDER BY `id_berita` DESC";
		$berita_umkm = $this->M_Berita->query($query_umkm);
		$i = 0;
		foreach ($berita_umkm as $key => $value) {
			$time_ago_umkm[$i++]=$this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		$data = array('data_berita' => $data_berita,
		'time_ago' => $time_ago,

		'berita_terkini' => $berita_terkini,
		'time_ago_terkini' => $time_ago_terkini,

		'berita_politik' => $berita_politik,
		'time_ago_politik' => $time_ago_politik,

		'berita_pemkot' => $berita_pemkot,
		'time_ago_pemkot' => $time_ago_pemkot,

		'berita_pendidikan' => $berita_pendidikan,
		'time_ago_pendidikan' => $time_ago_pendidikan,

		'berita_nasional' => $berita_nasional,
		'time_ago_nasional' => $time_ago_nasional,

		'berita_inspirasi' => $berita_inspirasi,
		'time_ago_inspirasi' => $time_ago_inspirasi,

		'berita_peristiwa' => $berita_peristiwa,
		'time_ago_peristiwa' => $time_ago_peristiwa,

		'berita_umkm' => $berita_umkm,
		'time_ago_umkm' => $time_ago_umkm);
		
		$this->load->view('beranda', $data);
	}

	public static function time_elapsed_string($datetime, $full = false) {
		$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);
	
		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;
	
		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}
	
		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}
