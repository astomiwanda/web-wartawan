<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function index()
    {
        if($this->session->userdata('status') == "login"){
			redirect(base_url('Dashboard'));
		}else{
            redirect(base_url('Beranda'));
        }
    }

     public function LoginAction()
	{
        if($this->input->post('login') == 'login'){
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $this->db->where('username', $username);
            $query = $this->db->get('user');
            $data = $query->row_array(); // get the row first

            if (!empty($data) && password_verify($password, $data['password'])) {
                // if this username exists, and the input password is verified using password_verify
                $data_session = array('id' => $data['id'], 'status' => "login");
                $this->session->set_userdata($data_session);
                redirect(base_url('Dashboard'));
            } else {
                echo "<script>alert('Username dan Password salah !');
                window.location.href='".base_url()."beranda';</script>";
            }
        }else {
            echo "<script>alert('Username dan Password Tidak Masuk !');
            window.location.href='".base_url()."beranda';</script>";
        }
    }
    
    public function RegistrationAction()
    {
        $data_username = array('username' => $this->input->post('username'));
		$cek = $this->M_user->cek_login('tuser', $data_username)->num_rows();
		$data = array('username' => $this->input->POST('username'),
						'password' => password_hash($this->input->POST('password'), PASSWORD_DEFAULT),
						'fullname' => $this->input->POST('fullname'),
						'level' => $this->input->POST('level') );

		if($cek == 1){
			$id = $this->input->POST('username');
			$data = array('username' => $this->input->POST('username'),
						'password' => password_hash($this->input->POST('password'), PASSWORD_DEFAULT),
						'fullname' => $this->input->POST('fullname'),
						'level' => $this->input->POST('level') );

			$post = $this->M_user->updateUser('tuser', $id, $data);
		}else{
			$post = $this->M_user->create('tuser', $data);
		}

		if ($post) {
			redirect(base_url('User'));
		}else{
			imap_alerts('gagal');
		}
    }

    public function logout(){
		$this->session->sess_destroy();
		redirect(base_url('Login'));
	}
}
