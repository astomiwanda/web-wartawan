<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NewsDescription extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($id='')
	{
		$query = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		ORDER BY `id_berita` DESC";
		$data_berita = $this->M_Berita->query($query);
		$i = 0;
		foreach ($data_berita as $key => $value) {
			$time_ago[$i++]= $this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu);
		}

		if ($id != '') {
			$query = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
			FROM `berita`
			LEFT JOIN `user` AS `user`
			ON `berita`.`user_id` = `user`.`id`
			LEFT JOIN `kategori` AS `kategori`
			ON `berita`.`kategori_id` = `kategori`.`id`
			WHERE `berita`.id = $id";
			
			$data = array('detail_berita' => $this->M_Berita->query($query),
			'data_berita' => $data_berita );
			$this->load->view('newsDescription', $data);
		}else{
			redirect(base_url('beranda'));
		}
	}
}
