<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('status') != "login"){
			redirect(base_url('Beranda'));
		}
    }
	
		/*Berita*/
	public function index()
	{
		$this->M_Berita->where('id', $this->session->userdata('id'));
		$user = $this->M_Berita->get('user');
		$query = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`";
    $data = array('data_berita' => $this->M_Berita->query($query),
    'user' => $user );
		$this->load->view('dashboard', $data);
    }

    public function ViewAddBerita()
    {
		$data = array('user' => $this->M_Berita->get('user') );
        $this->load->view('addBerita', $data);
	}
	
	public function AddActionBerita()
	{
		if ($this->input->post('tambah_berita')) {
			$config['upload_path']    = './assets/uploads/';
			$config['allowed_types']  = 'gif|jpg|png';
			$config['max_size']       = 10000;
			// $config['overwrite'] 			= TRUE;
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;
			
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('berkas')){
				$error = array('error' => $this->upload->display_errors());
				echo "<script>alert(".$this->upload->display_errors().");
				window.location.href='".base_url()."Dashboard/ViewAddBerita';</script>";
				// $this->load->view('v_upload', $error);
			}else{
				$data = $this->upload->data();
				$data_berita = array('user_id' => $this->session->userdata('id'),
				'judul' => $this->input->post('judul'),
				'isi' => $this->input->post('isi'),
				'lokasi' => $this->input->post('lokasi'),
				'tema' => $this->input->post('tema'),
				'kategori_id' => $this->input->post('kategori'),
				'waktu' => date("H:i:s"),
				'tanggal' => date("Y-m-d"),
				'img' => $data['file_name'] );

				$insert = $this->db->insert('berita', $data_berita);
				if ($insert) {
					echo "<script>alert('Data Berhasil ditambahkan !');
					window.location.href='".base_url()."Dashboard';</script>";
				}else{
					echo "<script>alert('Data Gagal ditambahkan !');
					window.location.href='".base_url()."Dashboard/ViewAddBerita';</script>";
				}
			}
		}else{
			echo "<script>alert('Data Gagal dikirim !');
			window.location.href='".base_url()."Dashboard/ViewAddBerita';</script>";
		}		
	}

	public function ViewEditBerita($id='')
	{
		$this->M_Berita->where('id', $this->session->userdata('id'));
		$user = $this->M_Berita->get('user');
		$this->M_Berita->where('id', $id);
		$data_berita = $this->M_Berita->get('berita');
		$data = array('data_berita' => $data_berita, 'user' => $user );
		$this->load->view('editBerita', $data);
	}

	public function EditActionBerita()
	{
		$gambar = '';
		$config['upload_path']    = './assets/uploads/';
		$config['allowed_types']  = 'gif|jpg|png';
		$config['max_size']       = 10000;
		// $config['overwrite'] 			= TRUE;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->input->post('edit_berita')) {
			$id = $this->input->post('id');			
			if ($_FILES['berkas']['name'] != '') {
				$gambar = $_FILES['berkas']['name'];
				$this->M_Berita->where('id', $id);
				foreach ($this->M_Berita->get('berita') as $value) {
					if( file_exists("./assets/uploads/".$value->img)){
						unlink("./assets/uploads/".$value->img);
					}
				}

				if ( ! $this->upload->do_upload('berkas')){
					$error = array('error' => $this->upload->display_errors());
					echo "<script>alert(".$this->upload->display_errors().");
					window.location.href='".base_url()."Dashboard/ViewAddBerita';</script>";
					// $this->load->view('v_upload', $error);
				}else{
					$nama_gambar = $this->upload->data();
					$gambar = $nama_gambar['file_name'];
				}

			}else {
				$this->M_Berita->where('id', $id);
				foreach ($this->M_Berita->get('berita') as $value) {
					$gambar = $value->img;
				}
			}

			$data = array('user_id' => $this->session->userdata('id'),
			'judul' => $this->input->post('judul'),
			'isi' => $this->input->post('isi'),
			'lokasi' => $this->input->post('lokasi'),
			'tema' => $this->input->post('tema'),
			'kategori_id' => $this->input->post('kategori'),
			'waktu' => date("H:i:s"),
			'tanggal' => date("Y-m-d"),
			'img' => $gambar );

			$insert = $this->M_Berita->update('berita', 'id', $id, $data);

			if ($insert) {
				echo "<script>alert('Data Berhasil diubah !');
				window.location.href='".base_url()."Dashboard/';</script>";
			}else{
				echo "<script>alert('Data Gagal diubah !');
				window.location.href='".base_url()."Dashboard/ViewEditBerita/$id';</script>";
			}
		}else {
			echo "<script>alert('Data Gagal dikirim !');
			window.location.href='".base_url()."Dashboard/ViewAddBerita';</script>";
		}
	}

	public function DeleteBerita($id='')
	{
		$gambar = '';
		$this->M_Berita->where('id', $id);
		$data_gambar = $this->M_Berita->get('berita');
		foreach ($data_gambar as $value) {
			$gambar = $value->img;
		}
		$data = $this->M_Berita->delete('berita', 'id', $id);
		if ($data) {
			unlink("./assets/uploads/".$gambar);
			echo "<script>alert('Data Berhasil dihapus !');
				window.location.href='".base_url()."Dashboard/';</script>";
		}else{
			echo "<script>alert('Data Gagal dihapus !');
				window.location.href='".base_url()."Dashboard/';</script>";
		}
	}

	public function DetailBerita($id='')
	{
		$this->M_Berita->where('id', $this->session->userdata('id'));
		$user = $this->M_Berita->get('user');
		$query = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		WHERE `berita`.id = '$id'";
    $detail_berita = $this->M_Berita->query($query);
		$data = array('user' => $user, 'detail_berita' => $detail_berita );
		$this->load->view('detailBerita', $data);
	}
	/*End of Berita*/

	/*Kategori Berita*/
	public function Kategori()
	{
		$this->M_Berita->where('id', $this->session->userdata('id'));
		$user = $this->M_Berita->get('user');
		$kategori = $this->M_Berita->get('kategori');
		$data = array('user' => $user, 'kategori' => $kategori );
		$this->load->view('kategoriBerita', $data);
	}

	public function ViewAddKategori()
	{
		$this->M_Berita->where('id', $this->session->userdata('id'));
		$user = $this->M_Berita->get('user');
		$data = array('user' => $user );
		$this->load->view('addKategoriBerita', $data);
	}

	public function AddActionKategori()
	{
		if ($this->input->post('tambah_kategori')) {
			$data = array('kategori' => $this->input->post('kategori') );
			$insert = $this->M_Berita->insert('kategori', $data);

			if ($insert) {
				echo "<script>alert('Data Berhasil ditambah !');
				window.location.href='".base_url()."Dashboard/Kategori';</script>";
			}else{
				echo "<script>alert('Data Gagal ditambah !');
				window.location.href='".base_url()."Dashboard/ViewAddKategori';</script>";
			}
		}else{
			echo "<script>alert('Data Tidak dikirim !');
				window.location.href='".base_url()."Dashboard/ViewAddKategori';</script>";
		}
	}

	public function ViewEditKategori($id='')
	{
		if ($id!='') {
			$this->M_Berita->where('id', $this->session->userdata('id'));
			$user = $this->M_Berita->get('user');
			$this->M_Berita->where('id', $id);
			$kategori = $this->M_Berita->get('kategori');
			$data = array('user' => $user, 'kategori' => $kategori );
		}else{
			echo "<script>alert('ID tidak ditemukan !');
				window.location.href='".base_url()."Dashboard/Kategori';</script>";
		}

		$this->load->view('editKategoriBerita', $data);
	}

	public function EditActionKategori()
	{
		if ($this->input->post('edit_kategori')) {
			$id = $this->input->post('id');
			$kategori = $this->input->post('kategori');
			$data = array('kategori' => $kategori );

			$insert = $this->M_Berita->update('kategori', 'id', $id, $data);
			
			if($insert){
				echo "<script>alert('Data Berhasl diubah !');
				window.location.href='".base_url()."Dashboard/Kategori';</script>";
			}else{
				echo "<script>alert('Data Gagal diubah !');
				window.location.href='".base_url()."Dashboard/ViewEditKategori/".$id."';</script>";
			}
		}else{
			echo "<script>alert('Data Tidak dikirim !');
				window.location.href='".base_url()."Dashboard/ViewEditKategori/".$id."';</script>";
		}
	}

	public function DeleteKategori($id='')
	{
		if ($id!='') {
			$delete = $this->M_Berita->delete('kategori', 'id', $id);
			if ($delete) {
				echo "<script>window.location.href='".base_url()."Dashboard/Kategori';</script>";
			}else{
				echo "<script>alert('Data Gagal dihapus !');
				window.location.href='".base_url()."Dashboard/Kategori';</script>";
			}
		}else{
			echo "<script>alert('ID tidak ditemukan !');
				window.location.href='".base_url()."Dashboard/Kategori';</script>";
		}
	}
	/* End of Kategori Berita*/

	/* User */
	public function User()
	{
		$this->M_Berita->where('id', $this->session->userdata('id'));
		$user = $this->M_Berita->get('user');
		$all_user = $this->M_Berita->get('user');

		$data = array('user' => $user, 'all_user' => $all_user );
		$this->load->view('user', $data);
	}

	public function ViewAddUser()
	{
		$this->M_Berita->where('id', $this->session->userdata('id'));
		$user = $this->M_Berita->get('user');

		$data = array('user' => $user );
		$this->load->view('addUser', $data);
	}

	public function AddActionUser()
	{
		if ($this->input->post('tambah_user')) {
			$config['upload_path']    = './assets/avatar/';
			$config['allowed_types']  = 'gif|jpg|png';
			$config['max_size']       = 10000;
			// $config['overwrite'] 			= TRUE;
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;
			
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('berkas')){
				$error = array('error' => $this->upload->display_errors());
				echo "<script>alert(".$this->upload->display_errors().");
				window.location.href='".base_url()."Dashboard/ViewAddUser';</script>";
				// $this->load->view('v_upload', $error);
			}else{
				$data = $this->upload->data();
				$data_berita = array('username' => $this->input->post('username'),
				'password' => password_hash($this->input->POST('password'), PASSWORD_DEFAULT),
				'fullname' => $this->input->post('fullname'),
				'avatar' => $data['file_name'],
				'level' => $this->input->post('level') );

				$insert = $this->db->insert('user', $data_berita);
				if ($insert) {
					echo "<script>alert('Data Berhasil ditambahkan !');
					window.location.href='".base_url()."Dashboard/User';</script>";
				}else{
					echo "<script>alert('Data Gagal ditambahkan !');
					window.location.href='".base_url()."Dashboard/ViewAddUser';</script>";
				}
			}
		}else{
			echo "<script>alert('Data Gagal dikirim !');
			window.location.href='".base_url()."Dashboard/ViewAddUser';</script>";
		}
	}

	public function ViewEditUser($id='')
	{
		$this->M_Berita->where('id', $this->session->userdata('id'));
		$user = $this->M_Berita->get('user');
		$this->M_Berita->where('id', $id);
		$data_user = $this->M_Berita->get('user');

		$data = array('user' => $user, 'data_user' => $data_user);
		$this->load->view('editUser', $data);
	}

	public function EditActionUser(Type $var = null)
	{
		$gambar = '';
		$config['upload_path']    = './assets/avatar/';
		$config['allowed_types']  = 'gif|jpg|png';
		$config['max_size']       = 10000;
		// $config['overwrite'] 			= TRUE;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->input->post('edit_user')) {
			$id = $this->input->post('id');			
			if ($_FILES['berkas']['name'] != '') {
				$gambar = $_FILES['berkas']['name'];
				$this->M_Berita->where('id', $id);
				foreach ($this->M_Berita->get('user') as $value) {
					if( file_exists("./assets/avatar/".$value->avatar)){
						unlink("./assets/avatar/".$value->avatar);
					}
				}

				if ( ! $this->upload->do_upload('berkas')){
					$error = array('error' => $this->upload->display_errors());
					echo "<script>alert(".$this->upload->display_errors().");
					window.location.href='".base_url()."Dashboard/ViewEditUser/".$id."';</script>";
					// $this->load->view('v_upload', $error);
				}else{
					$nama_gambar = $this->upload->data();
					$gambar = $nama_gambar['file_name'];
				}

			}else {
				$this->M_Berita->where('id', $id);
				foreach ($this->M_Berita->get('user') as $value) {
					$gambar = $value->avatar;
				}
			}

			$data_berita = array('username' => $this->input->post('username'),
			'fullname' => $this->input->post('fullname'),
			'avatar' => $gambar,
			'level' => $this->input->post('level') );

			$insert = $this->M_Berita->update('user', 'id', $id, $data_berita);

			if ($insert) {
				echo "<script>alert('Data Berhasil diubah !');
				window.location.href='".base_url()."Dashboard/User';</script>";
			}else{
				echo "<script>alert('Data Gagal diubah !');
				window.location.href='".base_url()."Dashboard/ViewEditUser/".$id."';</script>";
			}
		}else {
			echo "<script>alert('Data Gagal dikirim !');
			window.location.href='".base_url()."Dashboard/ViewEditUser/".$id."';</script>";
		}
	}

	public function DeleteUser($id='')
	{
		$gambar = '';
		$this->M_Berita->where('id', $id);
		$data_user = $this->M_Berita->get('user');
		foreach ($data_user as $value) {
			$gambar = $value->avatar;
		}
		$data = $this->M_Berita->delete('user', 'id', $id);
		if ($data) {
			unlink("./assets/avatar/".$gambar);
			echo "<script>alert('Data Berhasil dihapus !');
				window.location.href='".base_url()."Dashboard/';</script>";
		}else{
			echo "<script>alert('Data Gagal dihapus !');
				window.location.href='".base_url()."Dashboard/';</script>";
		}
	}
	/*End of User */
}
