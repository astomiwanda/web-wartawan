<html>
<head>
<title>Upload Form</title>
</head>
<body>

<h3>Your file was successfully uploaded!</h3>

<ul>
<?php foreach ($upload_data as $item => $value):?>
<li><?php echo $item;?>: <?php echo $value;?></li>
<?php endforeach; ?>
</ul>

<?php echo form_open_multipart('upload/aksi_upload');?>
 
	<input type="file" name="berkas" value="./assets/upload/<?php echo $value ?>" />
 
	<br /><br />
 
	<input type="submit" value="submit" />
 
</form>

<p><?php echo anchor('upload', 'Upload Another File!'); ?></p>

</body>
</html>