<?php $admin = false; foreach ($user as $value_user) {
  if ($value_user->level == 'Admin') { 
    $admin = true;
}} ?>
<?php require_once 'dashboard/top_navbar.php' ?>
<!-- Left side column. contains the logo and sidebar -->
<?php require_once 'dashboard/left_navbar.php' ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Data Berita
			<small>advanced tables</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Tables</a></li>
			<li class="active">Data Berita</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

    <?php foreach ($detail_berita as $key => $value) { ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="col-sm-6">
					<div class="box">
						<div class="box-header" style="padding: 25px;padding-bottom: 0px;">
							<h3 class="box-title">Detail Deskripsi Berita</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body" style="padding: 25px;">
							<div class="row" style="display: flex;align-items: center;margin-bottom: 8px;">
								<div class="col-sm-3">
									<h5 style="margin-top: 0px;margin-bottom: 0px; display: flex;justify-content: space-between;"><b>Judul</b><span>:</span></h5>
								</div>
								<div class="col-sm-9">
									<p style="margin-top: 0px;margin-bottom: 0px;"><?php echo $value->judul ?></p>
								</div>
							</div>

                            <div class="row" style="display: flex;align-items: center;margin-bottom: 8px;">
								<div class="col-sm-3">
									<h5 style="margin-top: 0px;margin-bottom: 0px; display: flex;justify-content: space-between;"><b>Penulis</b><span>:</span></h5>
								</div>
								<div class="col-sm-9">
									<p style="margin-top: 0px;margin-bottom: 0px;"><?php echo $value->fullname ?></p>
								</div>
							</div>

                            <div class="row" style="display: flex;align-items: center;margin-bottom: 8px;">
								<div class="col-sm-3">
									<h5 style="margin-top: 0px;margin-bottom: 0px; display: flex;justify-content: space-between;"><b>Lokasi</b><span>:</span></h5>
								</div>
								<div class="col-sm-9">
									<p style="margin-top: 0px;margin-bottom: 0px;"><?php echo $value->lokasi ?></p>
								</div>
							</div>

                            <div class="row" style="display: flex;align-items: center;margin-bottom: 8px;">
								<div class="col-sm-3">
									<h5 style="margin-top: 0px;margin-bottom: 0px; display: flex;justify-content: space-between;"><b>Tema</b><span>:</span></h5>
								</div>
								<div class="col-sm-9">
									<p style="margin-top: 0px;margin-bottom: 0px;"><?php echo $value->tema ?></p>
								</div>
							</div>

                            <div class="row" style="display: flex;align-items: center;margin-bottom: 8px;">
								<div class="col-sm-3">
									<h5 style="margin-top: 0px;margin-bottom: 0px; display: flex;justify-content: space-between;"><b>Tanggal</b><span>:</span></h5>
								</div>
								<div class="col-sm-9">
									<p style="margin-top: 0px;margin-bottom: 0px;"><?php echo $value->tanggal ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-6">
					<img src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>" alt="" style="width: 100%;height: 240px;object-fit: cover;">
				</div>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header" style="padding: 25px;padding-bottom: 0px;">
						<h3 class="box-title">Detail Deskripsi Berita</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body" style="padding: 25px;">
                        <h5><b>Deskrip Berita</b></h5>
                        <p><?php echo $value->isi ?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>Version</b> 2.4.0
	</div>
	<strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
	reserved.
</footer>

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?=base_url()?>assets/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>assets/AdminLTE/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url()?>assets/AdminLTE/dist/js/demo.js"></script>
<!-- page script -->
<script>
	$(function () {
		$('#example1').DataTable()
		$('#example2').DataTable()
		$(".treeview-tables").last().addClass("active");
		$(".menu-berita").last().addClass("active");
	})

</script>
</body>

</html>
