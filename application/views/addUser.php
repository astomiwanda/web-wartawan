<?php require_once 'dashboard/top_navbar.php' ?>
<?php require_once 'dashboard/left_navbar.php' ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Mepo Metropolitan
			<small>Tambah User Dashboard</small>
		</h1>
		<!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol> -->
	</section>

	<!-- Main content -->
	<section>
		<div class="content-header">
			<div class="row" style="margin-top: 50px;display: flex;justify-content: center;">
				<div class="col-sm-6">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">User Dashboard</h3>
						</div>
						<!-- /.box-header -->
						<!-- form start -->
						<form id="tambah_barang" action="<?=base_url()?>Dashboard/AddActionUser" method="post" class="form-horizontal"
						 enctype="multipart/form-data">
							<div class="box-body">
								<div class="form-group">
									<div class="col-sm-12">
										<label for="judul">Username</label>
										<input class="form-control" type="text" name="username" id="">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<label for="judul">Password</label>
										<input class="form-control" type="password" name="password" id="">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<label for="judul">fullname</label>
										<input class="form-control" type="text" name="fullname" id="">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<label for="judul">level</label>
                                        <select name="level" id="level" class="form-control">
                                            <option value="Admin">Admin</option>
                                            <option value="User">User</option>
                                        </select>
									</div>
								</div>
								<div class="form-group">
                                    <div class="col-sm-12">
									    <label for="exampleInputFile">Avatar</label>
									    <input type="file" name="berkas" id="exampleInputFile" class="form-control">
									    <p class="help-block">Max 2MB.</p>
                                    </div>
								</div>
							</div>
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="button" onclick="window.location.href='<?=base_url()?>Dashboard/User'" class="btn btn-default">Cancel</button>
								<button type="submit" name="tambah_user" value="tambah_user" class="btn btn-info pull-right">Tambah</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>
					<!-- /.box -->
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php require_once 'dashboard/footer.php' ?>
<script type="text/javascript">
	$(".treeview-tables").last().addClass("active");
	$(".menu-user").last().addClass("active");

</script>
