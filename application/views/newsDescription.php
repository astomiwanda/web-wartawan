<?php
foreach ($detail_berita as $key => $value) {
	$id_kategori = $value->kategori_id;
}

$query = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
		FROM `berita`
		LEFT JOIN `user` AS `user`
		ON `berita`.`user_id` = `user`.`id`
		LEFT JOIN `kategori` AS `kategori`
		ON `berita`.`kategori_id` = `kategori`.`id`
		WHERE `berita`.`kategori_id` = $id_kategori
		ORDER BY `id_berita` DESC";

$detail_berita_all = $this->M_Berita->query($query);

?>
<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8" />
	<title>News Description &#8211; Mepo</title>
	<?php require_once 'template/metacss.php' ?>
</head>

<body id="tie-body" class="post-template-default single single-post postid-7358 single-format-standard wrapper-has-shadow blocks-title-style-1 magazine1 is-thumb-overlay-disabled is-desktop is-header-layout-3 sidebar-right has-sidebar post-layout-1 narrow-title-narrow-media has-mobile-share post-has-toggle hide_share_post_top hide_share_post_bottom">
	<div class="background-overlay">
		<div id="tie-container" class="site tie-container">
			<div id="tie-wrapper">

				<?php require_once 'template/header.php' ?>
				<script type="text/javascript">
					var element = document.getElementById("menu-terkini");
					element.classList.add("tie-current-menu");

				</script>
				<div id="content" class="site-content container">
					<div class="tie-row main-content-row">
						<div class="main-content tie-col-md-8 tie-col-xs-12" role="main">
							<article id="the-post" class="container-wrapper post-content tie_standard">

								<?php foreach ($detail_berita as $key => $value) { ?>
								<header class="entry-header-outer">
									<nav id="breadcrumb"><a href="<?=base_url()?>Beranda"><span class="fa fa-home" aria-hidden="true"></span>
											Home</a><em class="delimiter">/</em><a href="<?=base_url()?>Terkini">
											<?php echo $value->kategori ?></a><em class="delimiter">/</em><span class="current">
											<?php echo $value->judul ?></span></nav>
									<div class="entry-header">
										<h5 class="post-cat-wrap"><a class="post-cat tie-cat-4" href="<?=base_url()?><?php echo $value->kategori ?>">
												<?php echo $value->kategori ?></a><a class="post-cat tie-cat-38" href="<?=base_url()?><?php echo $value->kategori ?>">
												<?php echo $value->tema ?></a></h5>
										<h1 class="post-title entry-title">
											<?php echo $value->judul ?>
										</h1>
										<div class="post-meta">
											<span class="meta-author-avatar">
												<!-- Foto Profile -->
												<a href="<?=base_url()?>Beranda"><img style="object-fit: cover;" src='<?=base_url()?>assets/avatar/<?php echo $value->avatar ?>'
													 class='avatar avatar-140 photo' height='140' width='140' />
												</a>
											</span>
											<span class="meta-author meta-item"><a href="<?=base_url()?>Beranda" class="author-name" title="Adie Rakasiwi">
													<?php echo $value->fullname ?></a>
											</span>
											<a href="mailto:hadieffendi2003@gmail.com" target="_blank">
												<span class="fa fa-envelope" aria-hidden="true"></span>
												<span class="screen-reader-text"></span>
											</a>
											<span class="date meta-item"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>
													<?php $date=date_create($value->tanggal); echo date_format($date,"F d, Y"); ?></span></span>
											<div class="tie-alignright"><span class="meta-comment meta-item"><a href="http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/#respond"><span
														 class="fa fa-comments" aria-hidden="true"></span> 0</a></span><span class="meta-views meta-item "><i
													 class="fa fa-eye" aria-hidden="true"></i> 34 </span> <span class="meta-reading-time meta-item"><span
													 class="fa fa-bookmark" aria-hidden="true"></span>
													<?php echo $this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu.''); ?></span> </div>
											<div class="clearfix"></div>
										</div><!-- .post-meta -->
									</div><!-- .entry-header /-->
								</header><!-- .entry-header-outer /-->
								<div class="post-footer post-footer-on-top">
									<div class="post-footer-inner">
										<div class="share-links  share-centered icons-only">
											<a href="http://www.facebook.com/sharer.php?u=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
											 rel="external" target="_blank" class="facebook-share-btn"><span class="fa fa-facebook"></span> <span class="screen-reader-text">Facebook</span></a><a
											 href="https://twitter.com/intent/tweet?text=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan&#038;url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
											 rel="external" target="_blank" class="twitter-share-btn"><span class="fa fa-twitter"></span> <span class="screen-reader-text">Twitter</span></a><a
											 href="https://plusone.google.com/_/+1/confirm?hl=en&#038;url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;name=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan"
											 rel="external" target="_blank" class="google-share-btn"><span class="fa fa-google"></span> <span class="screen-reader-text">Google+</span></a><a
											 href="http://www.linkedin.com/shareArticle?mini=true&#038;url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;title=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan"
											 rel="external" target="_blank" class="linkedin-share-btn"><span class="fa fa-linkedin"></span> <span class="screen-reader-text">LinkedIn</span></a><a
											 href="http://www.stumbleupon.com/submit?url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;title=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan"
											 rel="external" target="_blank" class="stumbleupon-share-btn"><span class="fa fa-stumbleupon"></span> <span
												 class="screen-reader-text">StumbleUpon</span></a><a href="http://www.tumblr.com/share/link?url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;name=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan"
											 rel="external" target="_blank" class="tumblr-share-btn"><span class="fa fa-tumblr"></span> <span class="screen-reader-text">Tumblr</span></a><a
											 href="http://pinterest.com/pin/create/button/?url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;description=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan&#038;media=http://depokpembaharuan.com/wp-content/uploads/2019/02/bersih.jpg"
											 rel="external" target="_blank" class="pinterest-share-btn"><span class="fa fa-pinterest"></span> <span
												 class="screen-reader-text">Pinterest</span></a><a href="http://reddit.com/submit?url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;title=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan"
											 rel="external" target="_blank" class="reddit-share-btn"><span class="fa fa-reddit"></span> <span class="screen-reader-text">Reddit</span></a><a
											 href="http://vk.com/share.php?url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
											 rel="external" target="_blank" class="vk-share-btn"><span class="fa fa-vk"></span> <span class="screen-reader-text">VKontakte</span></a><a
											 href="https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&#038;st.shareUrl=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;description=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan&#038;media=http://depokpembaharuan.com/wp-content/uploads/2019/02/bersih.jpg"
											 rel="external" target="_blank" class="odnoklassniki-share-btn"><span class="fa fa-odnoklassniki"></span>
												<span class="screen-reader-text">Odnoklassniki</span></a><a href="https://getpocket.com/save?title=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan&#038;url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
											 rel="external" target="_blank" class="pocket-share-btn"><span class="fa fa-get-pocket"></span> <span class="screen-reader-text">Pocket</span></a><a
											 href="whatsapp://send?text=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan - http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
											 rel="external" target="_blank" class="whatsapp-share-btn"><span class="fa fa-whatsapp"></span> <span class="screen-reader-text">WhatsApp</span></a><a
											 href="mailto:?subject=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan&#038;body=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
											 rel="external" target="_blank" class="email-share-btn"><span class="fa fa-envelope"></span> <span class="screen-reader-text">Share
													via Email</span></a><a href="#" rel="external" target="_blank" class="print-share-btn"><span class="fa fa-print"></span>
												<span class="screen-reader-text">Print</span></a> </div><!-- .share-links /-->
									</div><!-- .post-footer-inner /-->
								</div><!-- .post-footer-on-top /-->
								<div class="featured-area">
									<div class="featured-area-inner">
										<figure class="single-featured-image">
											<img width="700" height="400" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>" class="attachment-jannah-image-post size-jannah-image-post wp-post-image"
											 sizes="(max-width: 700px) 100vw, 700px" />
										</figure>
									</div>
								</div>
								<div class="entry-content entry clearfix">
									<p><strong>MEPO.COM
											<?PHP echo ' - '.$value->lokasi.' - ' ?></strong>
										<?php echo $value->isi ?>
									</p>
								</div><!-- .entry-content /-->
								<div class="toggle-post-content clearfix">
									<a id="toggle-post-button" class="button" href="#">
										Show More <span class="fa fa-chevron-down"></span>
									</a>
								</div><!-- .toggle-post-content -->
								<div class="post-footer post-footer-on-bottom">
									<div class="post-footer-inner">
										<div class="share-links  share-centered icons-only">
											<div class="share-title">
												<span class="fa fa-share-alt" aria-hidden="true"></span>
												<span> Share</span>
											</div>
											<a href="http://www.facebook.com/sharer.php?u=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
											 rel="external" target="_blank" class="facebook-share-btn"><span class="fa fa-facebook"></span> <span class="screen-reader-text">Facebook</span></a><a
											 href="https://twitter.com/intent/tweet?text=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan&#038;url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
											 rel="external" target="_blank" class="twitter-share-btn"><span class="fa fa-twitter"></span> <span class="screen-reader-text">Twitter</span></a><a
											 href="https://plusone.google.com/_/+1/confirm?hl=en&#038;url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;name=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan"
											 rel="external" target="_blank" class="google-share-btn"><span class="fa fa-google"></span> <span class="screen-reader-text">Google+</span></a><a
											 href="http://www.linkedin.com/shareArticle?mini=true&#038;url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;title=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan"
											 rel="external" target="_blank" class="linkedin-share-btn"><span class="fa fa-linkedin"></span> <span class="screen-reader-text">LinkedIn</span></a><a
											 href="http://www.stumbleupon.com/submit?url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;title=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan"
											 rel="external" target="_blank" class="stumbleupon-share-btn"><span class="fa fa-stumbleupon"></span> <span
												 class="screen-reader-text">StumbleUpon</span></a><a href="http://www.tumblr.com/share/link?url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;name=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan"
											 rel="external" target="_blank" class="tumblr-share-btn"><span class="fa fa-tumblr"></span> <span class="screen-reader-text">Tumblr</span></a><a
											 href="http://pinterest.com/pin/create/button/?url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;description=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan&#038;media=http://depokpembaharuan.com/wp-content/uploads/2019/02/bersih.jpg"
											 rel="external" target="_blank" class="pinterest-share-btn"><span class="fa fa-pinterest"></span> <span
												 class="screen-reader-text">Pinterest</span></a><a href="http://reddit.com/submit?url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;title=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan"
											 rel="external" target="_blank" class="reddit-share-btn"><span class="fa fa-reddit"></span> <span class="screen-reader-text">Reddit</span></a><a
											 href="http://vk.com/share.php?url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
											 rel="external" target="_blank" class="vk-share-btn"><span class="fa fa-vk"></span> <span class="screen-reader-text">VKontakte</span></a><a
											 href="https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&#038;st.shareUrl=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;description=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan&#038;media=http://depokpembaharuan.com/wp-content/uploads/2019/02/bersih.jpg"
											 rel="external" target="_blank" class="odnoklassniki-share-btn"><span class="fa fa-odnoklassniki"></span>
												<span class="screen-reader-text">Odnoklassniki</span></a><a href="https://getpocket.com/save?title=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan&#038;url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
											 rel="external" target="_blank" class="pocket-share-btn"><span class="fa fa-get-pocket"></span> <span class="screen-reader-text">Pocket</span></a><a
											 href="mailto:?subject=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan&#038;body=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
											 rel="external" target="_blank" class="email-share-btn"><span class="fa fa-envelope"></span> <span class="screen-reader-text">Share
													via Email</span></a><a href="#" rel="external" target="_blank" class="print-share-btn"><span class="fa fa-print"></span>
												<span class="screen-reader-text">Print</span></a>
										</div><!-- .share-links /-->
									</div><!-- .post-footer-inner /-->
								</div><!-- .post-footer-on-top /-->
								<?php } ?>

							</article><!-- #the-post /-->

							<div class="post-components">
								<div class="about-author container-wrapper" style='display: flex;align-items: center;'>
									<div class="author-avatar">
										<a href="http://depokpembaharuan.com/author/adie-rakasiwi/">
											<img src='<?=base_url()?>assets/avatar/<?php echo $value->avatar ?>' style='width: 80px;height: 80px;object-fit: cover;'
											 class='avatar avatar-180 photo' height='180' width='180' />
										</a>
									</div><!-- .author-avatar /-->
									<div class="author-info">
										<h3 class="author-name"><a href="http://depokpembaharuan.com/author/adie-rakasiwi/">
												<?php echo $value->fullname ?></a></h3>
										<div class="author-bio">
										</div><!-- .author-bio /-->
										<ul class="social-icons"></ul>
									</div><!-- .author-info /-->
									<div class="clearfix"></div>
								</div><!-- .about-author /-->
								<div class="container-wrapper" id="post-newsletter">
									<div class="subscribe-widget">
										<span class="fa fa-envelope newsletter-icon" aria-hidden="true"></span>
										<div class="subscribe-widget-content">
											<h4>With Product You Purchase</h4>
											<h3>Subscribe to our mailing list to get the new updates!</h3>
											<p>Lorem ipsum dolor sit amet, consectetur.</p>
										</div>
										<div id="mc_embed_signup">
											<form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="subscribe-form validate"
											 target="_blank" novalidate>
												<div id="mc_embed_signup_scroll">
													<div class="mc-field-group">
														<label class="screen-reader-text" for="mce-EMAIL">Enter your Email address</label>
														<input type="email" value="" id="mce-EMAIL" placeholder="Enter your Email address" name="EMAIL" class="subscribe-input required email"
														 id="mce-EMAIL">
													</div>
													<div id="mce-responses" class="clear">
														<div class="response" id="mce-error-response" style="display:none"></div>
														<div class="response" id="mce-success-response" style="display:none"></div>
													</div>
													<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button subscribe-submit">
												</div>
											</form>
										</div>
									</div><!-- .subscribe-widget /-->
								</div>

								<div class="prev-next-post-nav container-wrapper media-overlay">

									<?php $i=0; foreach ($detail_berita_all as $key => $value) { $i++;
									if ($i < 3) { ?>
									<div class="tie-col-xs-6 <?php if ($i %2 == 0) {echo " prev-post";} else {echo "next-post" ;} ?>">
										<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" style="background-image: url('<?=base_url()?>assets/uploads/<?php echo $value->img ?>')"
										 class="post-thumb" rel="prev">
											<div class="post-thumb-overlay-wrap">
												<div class="post-thumb-overlay">
													<span class="icon"></span>
												</div>
											</div>
										</a>
										<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" rel="prev">
											<h3 class="post-title">
												<?php echo word_limiter($value->judul, 8) ?>
											</h3>
										</a>
									</div>
									<?php }} ?>
								</div><!-- .prev-next-post-nav /-->
								<div id="related-posts" class="container-wrapper has-extra-post">
									<div class="mag-box-title the-global-title">
										<h3>Related Articles</h3>
									</div>
									<div class="related-posts-list">

										<?php $i=0; foreach ($data_berita as $key => $value) { $i++;
										if ($i < 5) { ?>
										<div class="related-item tie_standard">
											<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
											 title="<?php echo word_limiter($value->judul, 6) ?>" class="post-thumb">
												<div class="post-thumb-overlay-wrap">
													<div class="post-thumb-overlay">
														<span class="icon"></span>
													</div>
												</div>
												<img width="390" height="220" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>" class="attachment-jannah-image-large size-jannah-image-large wp-post-image"
												 alt="" />
											</a>
											<div class="post-meta"><span class="date meta-item"><i class="fa fa-clock-o" aria-hidden="true"></i>
													<span>
													<?php echo $this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu.''); ?></span></span>
												<div class="tie-alignright"><span class="meta-views meta-item "><i class="fa fa-eye" aria-hidden="true"></i>
														32 </span> </div>
												<div class="clearfix"></div>
											</div><!-- .post-meta -->
											<h3 class="post-title"><a href="http://depokpembaharuan.com/dandim-0508-depok-pimpin-apel-pasukan-pengamanan-kunjungan-presiden/"
												 title="Dandim 0508/Depok Pimpin Apel Pasukan Pengamanan Kunjungan Presiden">
													<?php echo $value->judul ?></a></h3>
										</div><!-- .related-item /-->
										<?php }} ?>

									</div><!-- .related-posts-list /-->
								</div><!-- #related-posts /-->
								<div id="comments" class="comments-area">
									<div id="add-comment-block" class="container-wrapper">
										<div id="respond" class="comment-respond">
											<h3 id="reply-title" class="comment-reply-title the-global-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link"
													 href="/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/#respond" style="display:none;">Cancel
														reply</a></small></h3>
											<form action="<?=base_url()?>" method="post" id="commentform" class="comment-form" novalidate>
												<p class="comment-notes"><span id="email-notes">Your email address will not be published.</span> Required
													fields are marked <span class="required">*</span></p>
												<p class="comment-form-comment"><label for="comment">Comment</label> <textarea id="comment" name="comment"
													 cols="45" rows="8" maxlength="65525" required="required"></textarea></p>
												<p class="comment-form-author"><label for="author">Name <span class="required">*</span></label> <input id="author"
													 name="author" type="text" value="" size="30" maxlength="245" required='required' /></p>
												<p class="comment-form-email"><label for="email">Email <span class="required">*</span></label> <input id="email"
													 name="email" type="email" value="" size="30" maxlength="100" aria-describedby="email-notes" required='required' /></p>
												<p class="comment-form-url"><label for="url">Website</label> <input id="url" name="url" type="url" value=""
													 size="30" maxlength="200" /></p>
												<p class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="Post Comment" />
													<input type='hidden' name='comment_post_ID' value='7358' id='comment_post_ID' />
													<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
												</p>
											</form>
										</div><!-- #respond -->
									</div><!-- #add-comment-block /-->
								</div><!-- .comments-area -->
							</div><!-- .post-components /-->
						</div><!-- .main-content -->
						<div id="check-also-box" class="container-wrapper check-also-right">
							<div class="widget-title the-global-title">
								<h4>Check Also</h4>
								<a href="#" id="check-also-close" class="remove">
									<span class="screen-reader-text">Close</span>
								</a>
							</div>
							<div class="widget posts-list-big-first" style="display: none">
								<ul class="posts-list-items">
									<li class="widget-post-list tie_standard">
										<div class="post-widget-thumbnail">
											<a href="http://depokpembaharuan.com/dandim-depok-setetes-darah-sangat-berarti/" title="Dandim Depok: Setetes Darah Sangat Berarti"
											 class="post-thumb">
												<h5 class="post-cat-wrap"><span class="post-cat tie-cat-4">
														<?php echo $tema ?></span></h5>
												<div class="post-thumb-overlay-wrap">
													<div class="post-thumb-overlay">
														<span class="icon"></span>
													</div>
												</div>
												<img width="390" height="220" src="<?=base_url()?>assets/img/bdb polri.PNG" class="attachment-jannah-image-large size-jannah-image-large wp-post-image"
												 alt="" sizes="(max-width: 390px) 100vw, 390px" />
											</a> </div><!-- post-alignleft /-->
										<div class="post-widget-body">
											<h3 class="post-title"><a href="http://depokpembaharuan.com/dandim-depok-setetes-darah-sangat-berarti/"
												 title="Dandim Depok: Setetes Darah Sangat Berarti">
													<?php echo $judul ?></a></h3>
											<div class="post-meta">
												<span class="date meta-item"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>1 day ago</span></span>
											</div>
										</div>
									</li>
								</ul><!-- .related-posts-list /-->
							</div>
						</div><!-- #related-posts /-->

						<?php require_once 'template/recent_popular.php' ?>

					</div><!-- .main-content-row /-->
				</div><!-- #content /-->

				<!-- Footer -->
				<?php require_once 'template/footer.php' ?>


				<div class="post-footer post-footer-on-mobile">
					<div class="post-footer-inner">
						<div class="share-links  icons-only">

							<a href="http://www.facebook.com/sharer.php?u=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
							 rel="external" target="_blank" class="facebook-share-btn"><span class="fa fa-facebook"></span> <span class="screen-reader-text">Facebook</span></a><a
							 href="https://twitter.com/intent/tweet?text=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan&#038;url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
							 rel="external" target="_blank" class="twitter-share-btn"><span class="fa fa-twitter"></span> <span class="screen-reader-text">Twitter</span></a><a
							 href="https://plusone.google.com/_/+1/confirm?hl=en&#038;url=http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/&#038;name=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan"
							 rel="external" target="_blank" class="google-share-btn"><span class="fa fa-google"></span> <span class="screen-reader-text">Google+</span></a><a
							 href="whatsapp://send?text=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan - http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
							 rel="external" target="_blank" class="whatsapp-share-btn"><span class="fa fa-whatsapp"></span> <span class="screen-reader-text">WhatsApp</span></a><a
							 href="tg://msg?text=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan - http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
							 rel="external" target="_blank" class="telegram-share-btn"><span class="fa fa-paper-plane"></span> <span class="screen-reader-text">Telegram</span></a><a
							 href="viber://forward?text=Antisipasi+DBD%2C+Polsek+Sawangan+Bareng+Masyarakat+Bersihkan+Selokan - http://depokpembaharuan.com/antisipasi-dbd-polsek-sawangan-bareng-masyarakat-bersihkan-selokan/"
							 rel="external" target="_blank" class="viber-share-btn"><span class="fa fa-volume-control-phone"></span> <span
								 class="screen-reader-text">Viber</span></a> </div><!-- .share-links /-->
					</div><!-- .post-footer-inner /-->
				</div><!-- .post-footer-on-top /-->

				<div class="mobile-share-buttons-spacer"></div>
				<a id="go-to-top" class="go-to-top-button" href="#go-to-tie-body"><span class="fa fa-angle-up"></span></a>
				<div class="clear"></div>

			</div><!-- #tie-wrapper /-->

			<?php require_once 'template/tab_mostpopular.php' ?>
		</div><!-- #tie-container /-->
	</div><!-- .background-overlay /-->

	<!-- Modal Login -->
	<?php require_once 'template/login.php' ?>

	<?php require_once 'template/metajs.php' ?>
</body>

</html>

<!-- Page generated by LiteSpeed Cache 2.8.1 on 2019-02-13 02:06:20 -->
