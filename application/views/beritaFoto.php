<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8" />
	<title>Berita Foto &#8211; Mepo</title>
	<?php require_once 'template/metacss.php' ?>
</head>


<body id="tie-body" class="archive category category-terkini category-4 wrapper-has-shadow blocks-title-style-1 magazine1 is-thumb-overlay-disabled is-desktop is-header-layout-3 sidebar-right has-sidebar hide_share_post_top hide_share_post_bottom">

	<div class="background-overlay">
		<div id="tie-container" class="site tie-container">
			<div id="tie-wrapper">

			<?php require_once 'template/header.php' ?>
			<script type="text/javascript">
				var element = document.getElementById("menu-berita-foto");
   				element.classList.add("tie-current-menu");
			</script>

				<div id="content" class="site-content container">
					<div class="tie-row main-content-row">

						<div class="main-content tie-col-md-8 tie-col-xs-12" role="main">

							<header class="entry-header-outer container-wrapper">
								<nav id="breadcrumb"><a href="http://depokpembaharuan.com/"><span class="fa fa-home" aria-hidden="true"></span>
										Home</a><em class="delimiter">/</em><span class="current">Berita Foto</span></nav>
								<h1 class="page-title">Berita Foto</h1>
							</header><!-- .entry-header-outer /-->


							<div class="mag-box wide-post-box">
								<div class="container-wrapper">
									<div class="mag-box-container clearfix">
										<ul id="posts-container" data-layout="default" data-settings="{'uncropped_image':'jannah-image-grid','category_meta':true,'post_meta':true,'excerpt':true,'excerpt_length':'20','read_more':true,'title_length':0,'is_full':false}"
										 class="posts-items">

										 <?php foreach ($data_berita_limit as $key => $value) { ?>
											<li class="post-item post-7647 post type-post status-publish format-standard has-post-thumbnail category-dawah category-ragam category-terkini tie_standard">
												<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo word_limiter($value->judul, 6) ?>"
												 class="post-thumb">
													<h5 class="post-cat-wrap"><span class="post-cat tie-cat-36"><?php echo $value->tema ?></span></h5>
													<div class="post-thumb-overlay-wrap">
														<div class="post-thumb-overlay">
															<span class="icon"></span>
														</div>
													</div>
													<img style="height: 240px;" width="390" height="220" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>"
													 class="attachment-jannah-image-large size-jannah-image-large wp-post-image" alt="" />
												</a>
												<div class="post-meta">
													<span class="meta-author meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" class="author-name"
														 title="<?php echo $value->fullname ?>"><span class="fa fa-user" aria-hidden="true"></span><?php echo " ".$value->fullname ?></a>
													</span>
													<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span><?php echo "$i hours ago" ?></span></span>
													<div class="tie-alignright"><span class="meta-comment meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"><span
																 class="fa fa-comments" aria-hidden="true"></span> 0</a></span><span class="meta-views meta-item "><i class="fa fa-eye" aria-hidden="true"></i> 12 </span> </div>
													<div class="clearfix"></div>
												</div><!-- .post-meta -->
												<div class="post-details">
													<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
														 title="<?php echo word_limiter($value->judul, 6) ?>"><?php echo word_limiter($value->judul, 8) ?></a></h3>

													<p class="post-excerpt">DEBAR.COM.<?php " - ".$value->lokasi." - " ?><?php echo word_limiter($value->isi, 24) ?></p>
													<a class="more-link" href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>">Read More
														&raquo;</a>
												</div>
											</li>
											<?php } ?>

										</ul><!-- #posts-container /-->
										<div class="clearfix"></div>
									</div><!-- .mag-box-container /-->
								</div><!-- .container-wrapper /-->
							</div><!-- .mag-box /-->
							<div class="pages-nav">
								<div class="pages-numbers pages-standard">
									<?php $page_prev = $page - 5; ?>
									<?php $page_next = $page + 5; ?>
									<span class="first-page first-last-pages">
										<?php if ($page >= 1) {?>
											<a href="<?=base_url()?>BeritaFoto/index/<?php echo $page_prev ?>"><span class="fa" aria-hidden="true" disabled></span>Prev
											page</a> </span>
										<?php } ?>
									</span>

									<span class="last-page first-last-pages">
										<?php if ($page < floor($jumlah_data/5)) { ?>
											<a href="<?=base_url()?>BeritaFoto/index/<?php echo $page_next ?>"><span class="fa" aria-hidden="true"></span>Next
											page</a> </span>
										<?php } ?>
								</div>
							</div>
						</div><!-- .main-content /-->


						<?php require_once 'template/recent_popular.php' ?>


					</div><!-- .main-content-row /-->
				</div><!-- #content /-->

				<?php require_once 'template/footer.php' ?>

				<a id="go-to-top" class="go-to-top-button" href="#go-to-tie-body"><span class="fa fa-angle-up"></span></a>
				<div class="clear"></div>
			</div><!-- #tie-wrapper /-->


			<?php require_once 'template/tab_mostpopular.php' ?>
		</div><!-- #tie-container /-->
	</div><!-- .background-overlay /-->

	<?php require_once 'template/login.php' ?>

	<?php require_once 'template/metajs.php' ?>
</body>

</html>

<!-- Page generated by LiteSpeed Cache 2.8.1 on 2019-02-14 10:16:15 -->
