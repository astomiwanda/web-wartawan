<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8" />
	<title>Beranda &#8211; Mepo</title>
	<?php require_once 'template/metacss.php' ?>
</head>


<body id="tie-body" class="home page-template-default page page-id-1020 wrapper-has-shadow blocks-title-style-1 magazine1 is-thumb-overlay-disabled is-desktop is-header-layout-3 has-builder hide_share_post_top hide_share_post_bottom">


	<div class="background-overlay">

		<div id="tie-container" class="site tie-container">


			<div id="tie-wrapper">

            <?php require_once 'template/header.php' ?>
			<script type="text/javascript">
				var element = document.getElementById("menu-beranda");
   				element.classList.add("tie-current-menu");
			</script>

				<div id="tiepost-1020-section-1741" class="section-wrapper container normal-width without-background">
					<div class="section-item full-width  is-first-section" style="">
						<div class="container-normal">
							<div class="tie-row main-content-row">
								<div class="main-content tie-col-md-12">
									<section id="tie-block_3336" class="slider-area mag-box">
										<div id="tie-main-slider-13-block_3336" class="tie-main-slider main-slider grid-5-big-centerd boxed-slider grid-slider-wrapper tie-slick-slider-wrapper"
										 data-slider-id="13" data-speed="3000">
											<div class="main-slider-wrapper">
												<div class="containerblock_3336">

													<div class="tie-slick-slider">
														<ul class="tie-slider-nav"></ul>

                                                        <div class="slide">
															<div style="background-image: url('<?=base_url()?>assets/uploads/<?php echo $data_berita[0]->img ?>')"
															 class="grid-item slide-id-7647 tie-slide-1 tie_standard">
																<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[0]->id_berita ?>" class="all-over-thumb-link"></a>
																<div class="thumb-overlay">
																	<div class="thumb-content">
																		<div class="thumb-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																				<span><?php echo $time_ago[0] ?></span></span></div>
																		<h2 class="thumb-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[0]->id_berita ?>"
																			 title="<?php print_r($data_berita[0]->judul); ?>"><?php print_r($data_berita[0]->judul); ?></a></h2>
																		<div class="thumb-desc">DEBAR.COM.<?php print_r(" ".$data_berita[0]->lokasi." - "); ?><?php echo word_limiter($data_berita[0]->isi, 32); ?></div><!-- .thumb-desc -->
																	</div> <!-- .thumb-content /-->
																</div><!-- .thumb-overlay /-->
															</div><!-- .slide || .grid-item /-->

															<div style="background-image: url('<?=base_url()?>assets/uploads/<?php echo $data_berita[1]->img ?>')"
															 class="grid-item slide-id-7643 tie-slide-2 tie_standard">
																<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[1]->id_berita ?>"
																 class="all-over-thumb-link"></a>
																<div class="thumb-overlay">
																	<div class="thumb-content">
																		<div class="thumb-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																				<span><?php echo $time_ago[1] ?></span></span></span></div>
																		<h2 class="thumb-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[1]->id_berita ?>"
																			 title="<?php print_r($data_berita[1]->judul); ?>"><?php print_r($data_berita[1]->judul); ?></a></h2>
																		<div class="thumb-desc">DEBAR.COM.<?php print_r(" ".$data_berita[1]->lokasi." - "); ?><?php echo word_limiter($data_berita[1]->isi, 24); ?></div><!-- .thumb-desc -->
																	</div> <!-- .thumb-content /-->
																</div><!-- .thumb-overlay /-->
															</div><!-- .slide || .grid-item /-->

															<div style="background-image: url('<?=base_url()?>assets/uploads/<?php echo $data_berita[2]->img ?>')"
															 class="grid-item slide-id-7639 tie-slide-3 tie_standard">
																<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[2]->id_berita ?>" class="all-over-thumb-link"></a>
																<div class="thumb-overlay">
																	<div class="thumb-content">
																		<div class="thumb-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																				<span><?php echo $time_ago[2] ?></span></span></div>
																		<h2 class="thumb-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[2]->id_berita ?>"
																			 title="<?php print_r($data_berita[2]->judul); ?>"><?php print_r($data_berita[2]->judul); ?></a></h2>
																		<div class="thumb-desc">DEBAR.COM.<?php print_r(" ".$data_berita[2]->lokasi." - "); ?><?php echo word_limiter($data_berita[2]->isi, 24); ?></div><!-- .thumb-desc -->
																	</div> <!-- .thumb-content /-->
																</div><!-- .thumb-overlay /-->
															</div><!-- .slide || .grid-item /-->

															<div style="background-image: url('<?=base_url()?>assets/uploads/<?php echo $data_berita[3]->img ?>')"
															 class="grid-item slide-id-7635 tie-slide-4 tie_standard">
																<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[3]->id_berita ?>" class="all-over-thumb-link"></a>
																<div class="thumb-overlay">
																	<div class="thumb-content">
																		<div class="thumb-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																				<span><?php echo $time_ago[3] ?></span></span></div>
																		<h2 class="thumb-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[3]->id_berita ?>"
																			 title="<?php print_r($data_berita[3]->judul); ?>"><?php print_r($data_berita[3]->judul); ?></a></h2>
																		<div class="thumb-desc">DEBAR.COM.<?php print_r(" ".$data_berita[3]->lokasi." - "); ?><?php echo word_limiter($data_berita[3]->isi, 24); ?></div><!-- .thumb-desc -->
																	</div> <!-- .thumb-content /-->
																</div><!-- .thumb-overlay /-->
															</div><!-- .slide || .grid-item /-->

															<div style="background-image: url('<?=base_url()?>assets/uploads/<?php echo $data_berita[4]->img ?>')"
															 class="grid-item slide-id-7629 tie-slide-5 tie_standard">
																<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[4]->id_berita ?>" class="all-over-thumb-link"></a>
																<div class="thumb-overlay">
																	<div class="thumb-content">
																		<div class="thumb-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																				<span><?php echo $time_ago[4] ?></span></span></div>
																		<h2 class="thumb-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[4]->id_berita ?>"
																			 title="<?php print_r($data_berita[4]->judul); ?>"><?php print_r($data_berita[4]->judul); ?></a></h2>
																		<div class="thumb-desc">DEBAR.COM.<?php print_r(" ".$data_berita[4]->lokasi." - "); ?><?php echo word_limiter($data_berita[4]->isi, 24); ?></div><!-- .thumb-desc -->
																	</div> <!-- .thumb-content /-->
																</div><!-- .thumb-overlay /-->
															</div><!-- .slide || .grid-item /-->
														</div> <!-- .slide -->

														<div class="slide">
															<div style="background-image: url('<?=base_url()?>assets/uploads/<?php echo $data_berita[5]->img ?>')"
															 class="grid-item slide-id-7647 tie-slide-1 tie_standard">
																<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[5]->id_berita ?>" class="all-over-thumb-link"></a>
																<div class="thumb-overlay">
																	<div class="thumb-content">
																		<div class="thumb-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																				<span><?php echo $time_ago[5] ?></span></span></div>
																		<h2 class="thumb-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[5]->id_berita ?>"
																			 title="<?php print_r($data_berita[5]->judul); ?>"><?php print_r($data_berita[5]->judul); ?></a></h2>
																		<div class="thumb-desc">DEBAR.COM.<?php print_r(" ".$data_berita[5]->lokasi." - "); ?><?php echo word_limiter($data_berita[5]->isi, 32); ?></div><!-- .thumb-desc -->
																	</div> <!-- .thumb-content /-->
																</div><!-- .thumb-overlay /-->
															</div><!-- .slide || .grid-item /-->

															<div style="background-image: url('<?=base_url()?>assets/uploads/<?php echo $data_berita[6]->img ?>')"
															 class="grid-item slide-id-7643 tie-slide-2 tie_standard">
																<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[6]->id_berita ?>"
																 class="all-over-thumb-link"></a>
																<div class="thumb-overlay">
																	<div class="thumb-content">
																		<div class="thumb-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																				<span><?php echo $time_ago[6] ?></span></span></span></div>
																		<h2 class="thumb-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[6]->id_berita ?>"
																			 title="<?php print_r($data_berita[6]->judul); ?>"><?php print_r($data_berita[6]->judul); ?></a></h2>
																		<div class="thumb-desc">DEBAR.COM.<?php print_r(" ".$data_berita[6]->lokasi." - "); ?><?php echo word_limiter($data_berita[6]->isi, 24); ?></div><!-- .thumb-desc -->
																	</div> <!-- .thumb-content /-->
																</div><!-- .thumb-overlay /-->
															</div><!-- .slide || .grid-item /-->

															<div style="background-image: url('<?=base_url()?>assets/uploads/<?php echo $data_berita[7]->img ?>')"
															 class="grid-item slide-id-7639 tie-slide-3 tie_standard">
																<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[7]->id_berita ?>" class="all-over-thumb-link"></a>
																<div class="thumb-overlay">
																	<div class="thumb-content">
																		<div class="thumb-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																				<span><?php echo $time_ago[7] ?></span></span></div>
																		<h2 class="thumb-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[7]->id_berita ?>"
																			 title="<?php print_r($data_berita[7]->judul); ?>"><?php print_r($data_berita[7]->judul); ?></a></h2>
																		<div class="thumb-desc">DEBAR.COM.<?php print_r(" ".$data_berita[7]->lokasi." - "); ?><?php echo word_limiter($data_berita[7]->isi, 24); ?></div><!-- .thumb-desc -->
																	</div> <!-- .thumb-content /-->
																</div><!-- .thumb-overlay /-->
															</div><!-- .slide || .grid-item /-->

															<div style="background-image: url('<?=base_url()?>assets/uploads/<?php echo $data_berita[8]->img ?>')"
															 class="grid-item slide-id-7635 tie-slide-4 tie_standard">
																<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[8]->id_berita ?>" class="all-over-thumb-link"></a>
																<div class="thumb-overlay">
																	<div class="thumb-content">
																		<div class="thumb-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																				<span><?php echo $time_ago[8] ?></span></span></div>
																		<h2 class="thumb-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[8]->id_berita ?>"
																			 title="<?php print_r($data_berita[8]->judul); ?>"><?php print_r($data_berita[8]->judul); ?></a></h2>
																		<div class="thumb-desc">DEBAR.COM.<?php print_r(" ".$data_berita[8]->lokasi." - "); ?><?php echo word_limiter($data_berita[8]->isi, 24); ?></div><!-- .thumb-desc -->
																	</div> <!-- .thumb-content /-->
																</div><!-- .thumb-overlay /-->
															</div><!-- .slide || .grid-item /-->

															<div style="background-image: url('<?=base_url()?>assets/uploads/<?php echo $data_berita[9]->img ?>')"
															 class="grid-item slide-id-7629 tie-slide-5 tie_standard">
																<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[9]->id_berita ?>" class="all-over-thumb-link"></a>
																<div class="thumb-overlay">
																	<div class="thumb-content">
																		<div class="thumb-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																				<span><?php echo $time_ago[9] ?></span></span></div>
																		<h2 class="thumb-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[9]->id_berita ?>"
																			 title="<?php print_r($data_berita[9]->judul); ?>"><?php print_r($data_berita[9]->judul); ?></a></h2>
																		<div class="thumb-desc">DEBAR.COM.<?php print_r(" ".$data_berita[9]->lokasi." - "); ?><?php echo word_limiter($data_berita[9]->isi, 24); ?></div><!-- .thumb-desc -->
																	</div> <!-- .thumb-content /-->
																</div><!-- .thumb-overlay /-->
															</div><!-- .slide || .grid-item /-->
														</div> <!-- .slide -->

													</div><!-- .tie-slick-slider /-->
												
                                                </div><!-- container /-->
											</div><!-- .main-slider-wrapper  /-->
										</div><!-- .main-slider /-->
									</section><!-- .slider-area -->
								</div><!-- .main-content /-->
							</div><!-- .main-content-row /-->
						</div><!-- .container /-->
					</div><!-- .section-item /-->
				</div><!-- .tiepost-1020-section-1741 /-->


				<div id="tiepost-1020-section-749" class="section-wrapper container normal-width without-background">
					<div class="section-item sidebar-left has-sidebar " style="">


						<div class="container-normal">
							<div class="tie-row main-content-row">
								<div class="main-content tie-col-md-8 tie-col-xs-12" role="main">

									<div id="tie-block_3151" class="mag-box big-post-left-box big-thumb-left-box has-custom-color" data-current="1">

										<div class="container-wrapper">


											<div class="mag-box-title the-global-title">
												<h3>Terkini</h3>
												<div class="tie-alignright">
													<div class="mag-box-options">

														<ul class="mag-box-filter-links">
															<li><a href="#" class="block-ajax-term active">All</a></li>
															<li><a href="#" data-id="4" class="block-ajax-term">Terkini</a></li>
														</ul>
														<ul class="slider-arrow-nav">
															<li><a class="block-pagination prev-posts pagination-disabled" href="#"><span class="fa fa-angle-left"
																	 aria-hidden="true"></span></a></li>
															<li><a class="block-pagination next-posts" href="#"><span class="fa fa-angle-right" aria-hidden="true"></span></a></li>
														</ul>

													</div><!-- .mag-box-options /-->
												</div><!-- .tie-alignright /-->
											</div><!-- .mag-box-title /-->
											<div class="mag-box-container clearfix">
												<ul class="posts-items posts-list-container">
													<li class="post-item tie_standard">
														<div class="big-thumb-left-box-inner" style="background-image: url('<?=base_url()?>assets/uploads/<?php echo $berita_terkini[0]->img ?>')">
															<a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_terkini[0]->id_berita ?>" title="<?php echo word_limiter($berita_terkini[0]->judul, 8) ?>"
															 class="post-thumb">
																<div class="post-thumb-overlay-wrap">
																	<div class="post-thumb-overlay">
																		<span class="icon"></span>
																	</div>
																</div>
															</a>
															<div class="post-overlay">
																<div class="post-content">
																	<h5 class="post-cat-wrap"><a class="post-cat tie-cat-36" href="<?=base_url()?>NewsDescription/index/<?php echo $berita_terkini[0]->id_berita ?>"><?php echo $berita_terkini[0]->tema ?></a></h5>
																	<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_terkini[0]->id_berita ?>"
																		 title="<?php print_r($berita_terkini[0]->judul); ?>"><?php print_r($berita_terkini[0]->judul); ?></a></h3>
																	<div class="thumb-meta">
																		<div class="post-meta">
																			<span class="meta-author meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_terkini[0]->id_berita ?>"
																				 class="author-name" title="<?php echo $berita_terkini[0]->fullname ?>"><span class="fa fa-user" aria-hidden="true"></span><?php echo $berita_terkini[0]->fullname ?></a>
																			</span>
																			<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span><?php echo $time_ago_terkini[0] ?></span></span>
																			<div class="tie-alignright"><span class="meta-comment meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_terkini[0]->id_berita ?>"><span
																						 class="fa fa-comments" aria-hidden="true"></span> 0</a></span><span class="meta-views meta-item "><i class="fa fa-eye" aria-hidden="true"></i> 11 </span> </div>
																			<div class="clearfix"></div>
																		</div><!-- .post-meta -->
																	</div>
																</div>
															</div>
														</div>
													</li><!-- .first-post -->
                                                    
													<?php $i = 0; foreach ($berita_terkini as $key => $value) { $i++;
													if ($i <= 4) { ?>
                                                    <li class="post-item tie_standard">
														<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
														 title="<?php echo $value->judul ?>" class="post-thumb">
															<div class="post-thumb-overlay-wrap">
																<div class="post-thumb-overlay">
																	<span class="icon"></span>
																</div>
															</div>
															<img width="220" height="150" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>"
															 class="attachment-jannah-image-small size-jannah-image-small wp-post-image" alt="" />
														</a>
														<div class="post-details">
															<div class="post-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																	<span><?php echo $time_ago_terkini[0] ?></span></span>
																<div class="clearfix"></div>
															</div><!-- .post-meta -->
															<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
																 title="<?php echo word_limiter($value->judul, 6) ?>"><?php echo word_limiter($value->judul, 10) ?></a></h3>
														</div><!-- .post-details /-->
													</li>
                                                    <?php }} ?>
													
												</ul>
												<div class="clearfix"></div>
											</div><!-- .mag-box-container /-->
										</div><!-- .container-wrapper /-->
									</div><!-- .mag-box /-->

									<script>var js_tie_block_3151 = {"order":"latest","id":["4"],"number":"6","pagi":"next-prev","excerpt":"true","post_meta":"true","read_more":"true","filters":"true","breaking_effect":"reveal","sub_style":"big_thumb","style":"big_thumb"};</script>

									<div id="tie-block_1160" class="mag-box big-post-top-box box-dark-skin dark-skin has-custom-color"
									 data-current="1">
										<div class="container-wrapper">
											<div class="mag-box-title the-global-title">
												<h3>Pemkot</h3>
												<div class="tie-alignright">
													<div class="mag-box-options">
														<ul class="mag-box-filter-links">
															<li><a href="#" class="block-ajax-term active">All</a></li>
															<li><a href="#" data-id="34" class="block-ajax-term">Pemkot</a></li>
														</ul>
														<ul class="slider-arrow-nav">
															<li><a class="block-pagination prev-posts pagination-disabled" href="#"><span class="fa fa-angle-left"
																	 aria-hidden="true"></span></a></li>
															<li><a class="block-pagination next-posts" href="#"><span class="fa fa-angle-right" aria-hidden="true"></span></a></li>
														</ul>
													</div><!-- .mag-box-options /-->
												</div><!-- .tie-alignright /-->
											</div><!-- .mag-box-title /-->
											<div class="mag-box-container clearfix">
												<ul class="posts-items posts-list-container">

													<li class="post-item tie_standard">
														<a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_pemkot[0]->id_berita ?>" title="<?php echo $berita_pemkot[0]->judul ?>"
														 class="post-thumb">
															<h5 class="post-cat-wrap"><span class="post-cat tie-cat-34"><?php echo $berita_pemkot[0]->tema ?></span></h5>
															<div class="post-thumb-overlay-wrap">
																<div class="post-thumb-overlay">
																	<span class="icon"></span>
																</div>
															</div>
															<img width="390" height="220" src="<?=base_url()?>assets/uploads/<?php echo $berita_pemkot[0]->img ?>"
															 class="attachment-jannah-image-large size-jannah-image-large wp-post-image" alt="" />
														</a>
														<div class="post-details">
															<div class="post-meta">
																<span class="meta-author meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_pemkot[0]->id_berita ?>" class="author-name"
																	 title="Adie Rakasiwi"><span class="fa fa-user" aria-hidden="true"></span><?php echo $berita_pemkot[0]->fullname ?></a>
																</span>
																<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span><?php echo $time_ago_pemkot[0] ?></span></span>
																<div class="tie-alignright"><span class="meta-comment meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_pemkot[0]->id_berita ?>"><span
																			 class="fa fa-comments" aria-hidden="true"></span> 0</a></span><span class="meta-views meta-item warm"><i class="fa fa-eye" aria-hidden="true"></i> 63 </span> </div>
																<div class="clearfix"></div>
															</div><!-- .post-meta -->
															<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_pemkot[0]->id_berita ?>"
																 title="<?php print_r($berita_pemkot[0]->judul); ?>"><?php print_r($berita_pemkot[0]->judul); ?></a></h3>
															<p class="post-excerpt">DEBAR.COM.<?php print_r(" ".$berita_pemkot[0]->lokasi." - "); ?><?php echo word_limiter($berita_pemkot[0]->isi, 32); ?></p>
															<a class="more-link" href="<?=base_url()?>NewsDescription/index/<?php echo $berita_pemkot[0]->id_berita ?>">Read
																More &raquo;</a>
														</div><!-- .post-details /-->
													</li><!-- .first-post -->
													
													<?php $i = -1; foreach ($berita_pemkot as $key => $value) { $i++;
														if ($i <= 3) { ?>
														<li class="post-item tie_standard">
														<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo $value->judul ?>"
														 class="post-thumb">
															<div class="post-thumb-overlay-wrap">
																<div class="post-thumb-overlay">
																	<span class="icon"></span>
																</div>
															</div>
															<img width="220" height="150" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>"
															 class="attachment-jannah-image-small size-jannah-image-small wp-post-image" alt="" />
														</a>
														<div class="post-details">
															<div class="post-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																	<span><?php echo $time_ago_pemkot[$i] ?></span></span>
																<div class="clearfix"></div>
															</div><!-- .post-meta -->
															<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
																 title="<?php echo word_limiter($value->judul, 6) ?>"><?php echo word_limiter($value->judul, 10) ?></a></h3>
														</div><!-- .post-details /-->
													</li>
													<?php }} ?>

												</ul>
												<div class="clearfix"></div>
											</div><!-- .mag-box-container /-->
										</div><!-- .container-wrapper /-->
									</div><!-- .mag-box /-->

									<script>var js_tie_block_1160 = {"order":"latest","id":["34"],"number":"5","pagi":"next-prev","dark":"true","excerpt":"true","excerpt_length":"22","post_meta":"true","read_more":"true","filters":"true","breaking_effect":"reveal","sub_style":"1c","style":"large_first"};</script>

									<div id="tie-block_1810" class="mag-box tie-col-sm-6 half-box has-custom-color" data-current="1">
										<div class="container-wrapper">
											<div class="mag-box-title the-global-title">
												<h3>Pendidikan</h3>
												<div class="tie-alignright">
													<div class="mag-box-options">
														<ul class="slider-arrow-nav">
															<li><a class="block-pagination prev-posts pagination-disabled" href="#"><span class="fa fa-angle-left"
																	 aria-hidden="true"></span></a></li>
															<li><a class="block-pagination next-posts" href="#"><span class="fa fa-angle-right" aria-hidden="true"></span></a></li>
														</ul>
													</div><!-- .mag-box-options /-->
												</div><!-- .tie-alignright /-->
											</div><!-- .mag-box-title /-->
											<div class="mag-box-container clearfix">
												<ul class="posts-items posts-list-container">
													<li class="post-item tie_standard">
														<a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_pendidikan[0]->id_berita ?>" title="<?php echo $berita_pendidikan[0]->judul ?>"
														 class="post-thumb">
															<h5 class="post-cat-wrap"><span class="post-cat tie-cat-35"><?php echo $berita_pendidikan[0]->tema ?></span></h5>
															<div class="post-thumb-overlay-wrap">
																<div class="post-thumb-overlay">
																	<span class="icon"></span>
																</div>
															</div>
															<img style="height: 180px;" width="390" height="220" src="<?=base_url()?>assets/uploads/<?php echo $berita_pendidikan[0]->img ?>"
															 class="attachment-jannah-image-large size-jannah-image-large wp-post-image" alt="" />
														</a>
														<div class="post-details">
															<div class="post-meta">
																<span class="meta-author meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_pendidikan[0]->id_berita ?>" class="author-name"
																	 title="Adie Rakasiwi"><span class="fa fa-user" aria-hidden="true"></span><?php echo " ".$berita_pendidikan[0]->fullname ?></a>
																</span>
																<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span><?php echo $time_ago_pendidikan[0] ?></span></span>
																<div class="tie-alignright"><span class="meta-comment meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_pendidikan[0]->id_berita ?>"><span
																			 class="fa fa-comments" aria-hidden="true"></span> 0</a></span><span class="meta-views meta-item "><i class="fa fa-eye" aria-hidden="true"></i> 50 </span> </div>
																<div class="clearfix"></div>
															</div><!-- .post-meta -->
															<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_pendidikan[0]->id_berita ?>"
																 title="<?php echo word_limiter($berita_pendidikan[0]->judul, 6) ?>"><?php echo word_limiter($berita_pendidikan[0]->judul, 16) ?></a></h3>

															<p class="post-excerpt">DEBAR.COM.<?php echo $berita_pendidikan[0]->lokasi." - " ?><?php echo word_limiter($berita_pendidikan[0]->isi, 32) ?></p>
															<a class="more-link" href="<?=base_url()?>NewsDescription/index/<?php echo $berita_pendidikan[0]->id_berita ?>">Read
																More &raquo;</a>
														</div><!-- .post-details /-->
													</li><!-- .first-post -->

													<?php $i=-1; foreach ($berita_pendidikan as $key => $value) { $i++;
													if ($i < 2) { ?>
													<li class="post-item tie_standard">
														<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo word_limiter($value->judul, 6) ?>"
														 class="post-thumb">
															<div class="post-thumb-overlay-wrap">
																<div class="post-thumb-overlay">
																	<span class="icon"></span>
																</div>
															</div>
															<img width="220" height="150" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>"
															 class="attachment-jannah-image-small size-jannah-image-small wp-post-image" alt="" />
														</a>
														<div class="post-details">
															<div class="post-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																	<span><?php echo $time_ago_pendidikan[$i] ?></span></span>
																<div class="clearfix"></div>
															</div><!-- .post-meta -->
															<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo word_limiter($value->judul, 6) ?>"><?php echo word_limiter($value->judul, 8) ?></a></h3>
														</div><!-- .post-details /-->
													</li>
													<?php }} ?>

												</ul>
												<div class="clearfix"></div>
											</div><!-- .mag-box-container /-->
										</div><!-- .container-wrapper /-->
									</div><!-- .mag-box /-->

									<script>var js_tie_block_1810 = {"order":"latest","id":["35"],"number":"3","pagi":"next-prev","excerpt":"true","excerpt_length":20,"post_meta":"true","read_more":"true","breaking_effect":"reveal","sub_style":"2c","style":"large_first"};</script>

									<div id="tie-block_304" class="mag-box tie-col-sm-6 half-box has-custom-color second-half-box" data-current="1">
										<div class="container-wrapper">
											<div class="mag-box-title the-global-title">
												<h3>Politik</h3>
												<div class="tie-alignright">
													<div class="mag-box-options">
														<ul class="slider-arrow-nav">
															<li><a class="block-pagination prev-posts pagination-disabled" href="#"><span class="fa fa-angle-left"
																	 aria-hidden="true"></span></a></li>
															<li><a class="block-pagination next-posts" href="#"><span class="fa fa-angle-right" aria-hidden="true"></span></a></li>
														</ul>
													</div><!-- .mag-box-options /-->
												</div><!-- .tie-alignright /-->
											</div><!-- .mag-box-title /-->
											<div class="mag-box-container clearfix">
												<ul class="posts-items posts-list-container">

													<li class="post-item tie_standard">
														<a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_politik[0]->id_berita ?>" title="<?php echo $berita_politik[0]->judul ?>"
														 class="post-thumb">
															<h5 class="post-cat-wrap"><span class="post-cat tie-cat-34"><?php echo $berita_politik[0]->tema ?></span></h5>
															<div class="post-thumb-overlay-wrap">
																<div class="post-thumb-overlay">
																	<span class="icon"></span>
																</div>
															</div>
															<img style="height: 180px;" width="390" height="220" src="<?=base_url()?>assets/uploads/<?php echo $berita_politik[0]->img ?>"
															 class="attachment-jannah-image-large size-jannah-image-large wp-post-image" alt="" />
														</a>
														<div class="post-details">
															<div class="post-meta">
																<span class="meta-author meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_politik[0]->id_berita ?>" class="author-name"
																	 title="Adie Rakasiwi"><span class="fa fa-user" aria-hidden="true"></span><?php echo " ".$berita_politik[0]->fullname ?></a>
																</span>
																<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span><?php echo $time_ago_politik[0] ?></span></span>
																<div class="tie-alignright"><span class="meta-comment meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_politik[0]->id_berita ?>"><span
																			 class="fa fa-comments" aria-hidden="true"></span> 0</a></span><span class="meta-views meta-item "><i class="fa fa-eye" aria-hidden="true"></i> 37 </span> </div>
																<div class="clearfix"></div>
															</div><!-- .post-meta -->
															<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_politik[0]->id_berita ?>"
																 title="<?php echo word_limiter($berita_politik[0]->judul,6) ?>"><?php echo word_limiter($berita_politik[0]->judul, 8) ?></a></h3>
															<p class="post-excerpt">DEBAR.COM.<?php echo " - ".$berita_politik[0]->lokasi." - " ?><?php echo word_limiter($berita_politik[0]->isi, 32) ?></p>
															<a class="more-link" href="<?=base_url()?>NewsDescription/index/<?php echo $berita_politik[0]->id_berita ?>">Read
																More &raquo;</a>
														</div><!-- .post-details /-->
													</li><!-- .first-post -->

													<?php $i=-1; foreach ($berita_politik as $key => $value) { $i++;
													if ($i < 2) { ?>
													<li class="post-item tie_standard">
														<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo word_limiter($value->judul, 6) ?>"
														 class="post-thumb">
															<div class="post-thumb-overlay-wrap">
																<div class="post-thumb-overlay">
																	<span class="icon"></span>
																</div>
															</div>
															<img width="220" height="150" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>"
															 class="attachment-jannah-image-small size-jannah-image-small wp-post-image" alt="" />
														</a>
														<div class="post-details">
															<div class="post-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																	<span><?php echo $time_ago_politik[$i] ?></span></span>
																<div class="clearfix"></div>
															</div><!-- .post-meta -->
															<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
																 title="<?php echo word_limiter($value->judul, 6) ?>"><?php echo word_limiter($value->judul, 8) ?></a></h3>
														</div><!-- .post-details /-->	
													</li>
													<?php }} ?>

												</ul>
												<div class="clearfix"></div>
											</div><!-- .mag-box-container /-->
										</div><!-- .container-wrapper /-->
									</div><!-- .mag-box /-->

									<script>var js_tie_block_304 = {"order":"latest","id":["11"],"number":"3","pagi":"next-prev","excerpt":"true","excerpt_length":20,"post_meta":"true","read_more":"true","breaking_effect":"reveal","sub_style":"2c","style":"large_first"};</script>
									
									<div class="clearfix half-box-clearfix" style="display: none;"></div>
									<div id="tie-block_2136" class="mag-box stream-item-mag stream-item" style="display: none;">
										<div class="container-wrapper">
											<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
											<!-- Metro Depok 728 x 90 -->
											<ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-5782662657508727"
											 data-ad-slot="9569679550"></ins>
											<script>
												(adsbygoogle = window.adsbygoogle || []).push({});
                                            </script>
										</div><!-- .container-wrapper /-->
									</div><!-- .mag-box /-->
								</div><!-- .main-content /-->

								<?php require_once 'template/recent_popular.php' ?>
							</div><!-- .main-content-row -->
						</div><!-- .container /-->
					</div><!-- .section-item /-->
				</div><!-- .tiepost-1020-section-749 /-->

				<div id="tiepost-1020-section-6559" class="section-wrapper container normal-width without-background">
					<div class="section-item sidebar-left has-sidebar " style="">
						<div class="container-normal">
							<div class="tie-row main-content-row">
								<div class="main-content tie-col-md-8 tie-col-xs-12" role="main">
									<div id="tie-block_611" class="mag-box big-post-top-box has-custom-color" data-current="1">
										<div class="container-wrapper">
											<div class="mag-box-title the-global-title">
												<h3>Nasional</h3>
												<div class="tie-alignright">
													<div class="mag-box-options">
														<ul class="mag-box-filter-links">
															<li><a href="#" class="block-ajax-term active">All</a></li>
															<li><a href="#" data-id="37" class="block-ajax-term">Nasional</a></li>
														</ul>
														<ul class="slider-arrow-nav">
															<li><a class="block-pagination prev-posts pagination-disabled" href="#"><span class="fa fa-angle-left"
																	 aria-hidden="true"></span></a></li>
															<li><a class="block-pagination next-posts" href="#"><span class="fa fa-angle-right" aria-hidden="true"></span></a></li>
														</ul>
													</div><!-- .mag-box-options /-->
												</div><!-- .tie-alignright /-->
											</div><!-- .mag-box-title /-->
											<div class="mag-box-container clearfix">
												<ul class="posts-items posts-list-container">

													<li class="post-item tie_standard">
														<a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_nasional[0]->id_berita ?>"
														 title="Dandim 0508/Depok Pimpin Apel Pasukan Pengamanan Kunjungan Presiden" class="post-thumb">
															<h5 class="post-cat-wrap"><span class="post-cat tie-cat-37"><?php echo word_limiter($berita_nasional[0]->tema) ?></span></h5>
															<div class="post-thumb-overlay-wrap">
																<div class="post-thumb-overlay">
																	<span class="icon"></span>
																</div>
															</div>
															<img style="height: 180px;" width="390" height="220" src="<?=base_url()?>assets/uploads/<?php echo $berita_nasional[0]->img ?>"
															 class="attachment-jannah-image-large size-jannah-image-large wp-post-image" alt="" />
														</a>
														<div class="post-details">

															<div class="post-meta">
																<span class="meta-author meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_nasional[0]->id_berita ?>" class="author-name"
																	 title="Adie Rakasiwi"><span class="fa fa-user" aria-hidden="true"></span><?php echo " ".$berita_nasional[0]->fullname ?></a>
																</span>
																<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span><?php echo $time_ago_nasional[0] ?></span></span>
																<div class="tie-alignright"><span class="meta-comment meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_nasional[0]->id_berita ?>"><span
																			 class="fa fa-comments" aria-hidden="true"></span> 0</a></span><span class="meta-views meta-item "><i class="fa fa-eye" aria-hidden="true"></i> 41 </span> </div>
																<div class="clearfix"></div>
															</div><!-- .post-meta -->
															<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $berita_nasional[0]->id_berita ?>"
																 title="<?php echo word_limiter($berita_nasional[0]->judul, 6) ?>"><?php echo word_limiter($berita_nasional[0]->judul, 12) ?></a></h3>
															<p class="post-excerpt">DEBAR.COM.<?php echo " - ".$berita_nasional[0]->lokasi." - " ?><?php echo word_limiter($berita_nasional[0]->isi, 32) ?></p>
															<a class="more-link" href="<?=base_url()?>NewsDescription/index/<?php echo $berita_nasional[0]->id_berita ?>">Read
																More &raquo;</a>
														</div><!-- .post-details /-->
													</li><!-- .first-post -->

													<?php $i=-1; foreach ($berita_nasional as $key => $value) { $i++;
													if ($i < 4) { ?>
													<li class="post-item tie_standard">
														<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
														 title="<?php echo word_limiter($value->judul, 6) ?>" class="post-thumb">
															<div class="post-thumb-overlay-wrap">
																<div class="post-thumb-overlay">
																	<span class="icon"></span>
																</div>
															</div>
															<img width="220" height="150" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>"
															 class="attachment-jannah-image-small size-jannah-image-small wp-post-image" alt="" />
														</a>
														<div class="post-details">
															<div class="post-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																	<span><?php echo $time_ago_nasional[$i] ?></span></span>
																<div class="clearfix"></div>
															</div><!-- .post-meta -->
															<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
																 title="<?php echo word_limiter($value->judul, 6) ?>">	<?php echo word_limiter($value->judul, 8) ?></a></h3>
														</div><!-- .post-details /-->
													</li>
													<?php }} ?>

												</ul>
												<div class="clearfix"></div>
											</div><!-- .mag-box-container /-->
										</div><!-- .container-wrapper /-->
									</div><!-- .mag-box /-->

									<script>var js_tie_block_611 = {"order":"latest","id":["37"],"number":"5","pagi":"next-prev","excerpt":"true","excerpt_length":"22","more":"true","post_meta":"true","read_more":"true","filters":"true","breaking_effect":"reveal","sub_style":"1c","style":"large_first"};</script>

									<div id="tie-block_1837" class="mag-box big-post-top-box media-overlay has-custom-color" data-current="1">
										<div class="container-wrapper">
											<div class="mag-box-title the-global-title">
												<h3>Da'wah</h3>
												<div class="tie-alignright">
													<div class="mag-box-options">
														<ul class="slider-arrow-nav">
															<li><a class="block-pagination prev-posts pagination-disabled" href="#"><span class="fa fa-angle-left"
																	 aria-hidden="true"></span></a></li>
															<li><a class="block-pagination next-posts" href="#"><span class="fa fa-angle-right" aria-hidden="true"></span></a></li>
														</ul>
													</div><!-- .mag-box-options /-->
												</div><!-- .tie-alignright /-->
											</div><!-- .mag-box-title /-->
											<div class="mag-box-container clearfix">
												<ul class="posts-items posts-list-container">
													
													<?php $i=0; foreach ($data_berita as $key => $value) {
														if ($value->tema == "Da'wah" && $i == 0) { $i = 1; ?>
														<li class="post-item tie_standard">
														<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo word_limiter($value->judul, 6) ?>"
														 class="post-thumb">
															<h5 class="post-cat-wrap"><span class="post-cat tie-cat-36"><?php echo $value->tema ?></span></h5>
															<div class="post-thumb-overlay-wrap">
																<div class="post-thumb-overlay">
																	<span class="icon"></span>
																</div>
															</div>
															<img style="height: 180px;" width="390" height="220" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>"
															 class="attachment-jannah-image-large size-jannah-image-large wp-post-image" alt="" />
														</a>
														<div class="post-details">
															<div class="post-meta">
																<span class="meta-author meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" class="author-name"
																	 title="Adie Rakasiwi"><span class="fa fa-user" aria-hidden="true"></span><?php echo " ".$value->fullname ?></a>
																</span>
																<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span><?php echo Beranda::time_elapsed_string(''.$value->tanggal.' '.$value->waktu.'') ?></span></span>
																<div class="tie-alignright"><span class="meta-comment meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"><span
																			 class="fa fa-comments" aria-hidden="true"></span> 0</a></span><span class="meta-views meta-item "><i class="fa fa-eye" aria-hidden="true"></i> 11 </span> </div>
																<div class="clearfix"></div>
															</div><!-- .post-meta -->
															<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
																 title="<?php echo word_limiter($value->judul, 6) ?>"><?php echo word_limiter($value->judul, 8) ?></a></h3>
															<p class="post-excerpt">DEBAR.COM.<?php echo " - ".$value->lokasi." - " ?><?php echo word_limiter($value->isi, 32) ?></p>
															<a class="more-link" href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>">Read
																More &raquo;</a>
														</div><!-- .post-details /-->
													</li><!-- .first-post -->
														<?php }} ?>

													<?php $i=-1; foreach ($data_berita as $key => $value) {
													if ($value->tema == "Da'wah") { $i++; ?>
													<li class="post-item tie_standard">
														<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
														 title="<?php echo word_limiter($value->judul, 6) ?>" class="post-thumb">
															<div class="post-thumb-overlay-wrap">
																<div class="post-thumb-overlay">
																	<span class="icon"></span>
																</div>
															</div>
															<img width="220" height="150" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>"
															 class="attachment-jannah-image-small size-jannah-image-small wp-post-image" alt="" />
														</a>
														<div class="post-details">
															<div class="post-meta"><span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span>
																	<span><?php echo Beranda::time_elapsed_string(''.$value->tanggal.' '.$value->waktu.'') ?></span></span>
																<div class="clearfix"></div>
															</div><!-- .post-meta -->
															<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
																 title="<?php echo word_limiter($value->judul, 6) ?>"><?php echo word_limiter($value->judul, 8) ?></a></h3>
														</div><!-- .post-details /-->
													</li>
													<?php }} ?>

												</ul>
												<div class="clearfix"></div>
											</div><!-- .mag-box-container /-->
										</div><!-- .container-wrapper /-->
									</div><!-- .mag-box /-->

									<script>var js_tie_block_1837 = {"order":"latest","id":["36"],"number":"5","pagi":"next-prev","excerpt":"true","excerpt_length":15,"post_meta":"true","media_overlay":"true","read_more":"true","breaking_effect":"reveal","sub_style":"1c","style":"large_first"};</script>

									<div id="tie-block_2223" class="mag-box stream-item-mag stream-item content-only">
										<div class="container-wrapper">
											<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
											<!-- Metro Depok 728 x 90 -->
											<ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-5782662657508727"
											 data-ad-slot="9569679550"></ins>
											<script>
												(adsbygoogle = window.adsbygoogle || []).push({});</script>
										</div><!-- .container-wrapper /-->
									</div><!-- .mag-box /-->

									<div id="tie-s_1441" class="mag-box big-posts-box has-custom-color" data-current="1">
										<div class="container-wrapper">
											<div class="mag-box-title the-global-title">
												<h3>UMKM</h3>
											</div><!-- .mag-box-title /-->
											<div class="mag-box-container clearfix">
												<ul class="posts-items posts-list-container">

													<?php $i=-1; foreach ($berita_umkm as $key => $value) { $i++;
													if ($i < 4) { ?>
													<li class="post-item post-7534 post type-post status-publish format-standard has-post-thumbnail category-ragam category-terkini category-umkm tie_standard">
														<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo word_limiter($value->judul, 6) ?>"
														 class="post-thumb">
															<h5 class="post-cat-wrap"><span class="post-cat tie-cat-8"><?php echo $value->tema ?></span></h5>
															<div class="post-thumb-overlay-wrap">
																<div class="post-thumb-overlay">
																	<span class="icon"></span>
																</div>
															</div>
															<img width="390" height="220" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>"
															 class="attachment-jannah-image-large size-jannah-image-large wp-post-image" alt="" />
														</a>
														<div class="post-meta">
															<span class="meta-author meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" class="author-name"
																 title="<?php echo $value->fullname ?>"><span class="fa fa-user" aria-hidden="true"></span><?php echo " ".$value->fullname ?></a>
															</span>
															<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span><?php echo $time_ago_umkm[$i] ?></span></span>
															<div class="tie-alignright"><span class="meta-comment meta-item"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"><span
																		 class="fa fa-comments" aria-hidden="true"></span> 0</a></span><span class="meta-views meta-item warm"><i class="fa fa-eye" aria-hidden="true"></i> 66 </span> </div>
															<div class="clearfix"></div>
														</div><!-- .post-meta -->
														<div class="post-details">
															<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
																 title="<?php echo word_limiter($value->judul, 6) ?>"><?php echo word_limiter($value->judul, 8) ?></a></h3>
															<p class="post-excerpt">DEBAR.COM.<?php echo " - ".$value->lokasi." - " ?><?php echo word_limiter($value->isi, 32) ?></p>
														</div>
													</li>
													<?php }} ?>

												</ul>
												<div class="clearfix"></div>
											</div><!-- .mag-box-container /-->
											<a class="block-pagination next-posts show-more-button load-more-button" data-text="Load More">Load More</a>
										</div><!-- .container-wrapper /-->
									</div><!-- .mag-box /-->

									<script>var js_tie_s_1441 = {"order":"latest","id":["7"],"number":"4","pagi":"load-more","excerpt":"true","post_meta":"true","breaking_effect":"reveal","sub_style":"big","style":"default"};</script>

								</div><!-- .main-content /-->
								<aside class="sidebar tie-col-md-4 tie-col-xs-12 normal-side is-sticky" aria-label="Primary Sidebar">
									<?php require_once 'template/mostviewed.php' ?>
									<?php require_once 'template/category_num.php' ?>
								</aside><!-- .sidebar /-->

							</div><!-- .main-content-row -->
						</div><!-- .container /-->
					</div><!-- .section-item /-->
				</div><!-- .tiepost-1020-section-6559 /-->

				<?php require_once 'template/footer.php' ?>

				<a id="go-to-top" class="go-to-top-button" href="#go-to-tie-body"><span class="fa fa-angle-up"></span></a>
				<div class="clear"></div>
			</div><!-- #tie-wrapper /-->

			<?php require_once 'template/tab_mostpopular.php' ?>
		</div><!-- #tie-container /-->
	</div><!-- .background-overlay /-->

	<?php require_once 'template/login.php' ?>

	<?php require_once 'template/metajs.php' ?>
</body>
</html>

<!-- Page generated by LiteSpeed Cache 2.8.1 on 2019-02-14 09:29:40 -->
