<?php require_once 'dashboard/top_navbar.php' ?>
<?php require_once 'dashboard/left_navbar.php' ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Mepo Metropolitan
			<small>Tambah Kategori Berita</small>
		</h1>
		<!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol> -->
	</section>

	<!-- Main content -->
	<section>
		<div class="content-header">
			<div class="row" style="margin-top: 50px;display: flex;justify-content: center;">
				<div class="col-sm-6">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">Kategori Berita</h3>
						</div>
						<!-- /.box-header -->
						<!-- form start -->
						<form id="tambah_barang" action="<?=base_url()?>Dashboard/EditActionKategori" method="post" class="form-horizontal"
						 enctype="multipart/form-data">
							<div class="box-body">
                            <?php foreach ($kategori as $key => $value) {?>
                                <div class="form-group">
									<div class="col-sm-12">
                                        <input type="hidden" name="id" value="<?php echo $value->id ?>">
										<label for="judul">Kategori</label>
										<input class="form-control" type="text" name="kategori" id="" value="<?php echo $value->kategori ?>">
									</div>
								</div>
                            <?php } ?>
							</div>
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="button" onclick="window.location.href='<?=base_url()?>Dashboard/Kategori'" class="btn btn-default">Cancel</button>
								<button type="submit" name="edit_kategori" value="edit_kategori" class="btn btn-info pull-right">Edit</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>
					<!-- /.box -->
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php require_once 'dashboard/footer.php' ?>
<script type="text/javascript">
	$(".treeview-tables").last().addClass("active");
	$(".menu-kategori").last().addClass("active");

</script>
