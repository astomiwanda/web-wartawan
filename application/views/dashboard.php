<?php $admin = false; foreach ($user as $value_user) {
  if ($value_user->level == 'Admin') { 
    $admin = true;
}} ?>
<?php require_once 'dashboard/top_navbar.php' ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php require_once 'dashboard/left_navbar.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Berita
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data Berita</li>
      </ol>
    </section>

    <!-- Main content -->
    <div style="display: flex; align-items: center; margin: 15px 0 0 15px;">
      <a href="<?=base_url()?>Dashboard/ViewAddBerita" type="button" class="btn btn-success">Tambah</a>
    </div>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tabel Data Berita</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                	<th>No</th>
                	<th>Judul</th>
                	<th>Isi</th>
                	<th>Lokasi</th>
                    <th>Tema</th>
                    <th>Kategori</th>
                    <th>Tanggal</th>
                    <th>Img</th>
                	<?php $admin = false; foreach ($user as $value) {
                		if ($value->level == 'Admin') {
                      $admin = true; ?>
                			<th>Edit</th>
                      <th>Hapus</th>
                	<?php }} ?>
                </tr>
                </thead>
                <tbody>
            	<?php $i=0; foreach ($data_berita as $value)
            	{ $i++;?>
            		<tr>
                  <td><a href="<?=base_url()?>Dashboard/DetailBerita/<?php echo $value->id_berita ?>" style="color: #333333;"><?php echo $i;?></td></a>
            			<td><a href="<?=base_url()?>Dashboard/DetailBerita/<?php echo $value->id_berita ?>" style="color: #333333;"><?php echo word_limiter($value->judul , 8); ?></td></a>
            			<td><a href="<?=base_url()?>Dashboard/DetailBerita/<?php echo $value->id_berita ?>" style="color: #333333;"><?php echo word_limiter($value->isi , 16); ?></td></a>
            			<td><a href="<?=base_url()?>Dashboard/DetailBerita/<?php echo $value->id_berita ?>" style="color: #333333;"><?php echo $value->lokasi;?></td></a>
                  <td><a href="<?=base_url()?>Dashboard/DetailBerita/<?php echo $value->id_berita ?>" style="color: #333333;"><?php echo $value->tema;?></td></a>
                  <td><a href="<?=base_url()?>Dashboard/DetailBerita/<?php echo $value->id_berita ?>" style="color: #333333;"><?php echo $value->kategori;?></td></a>
                  <td><a href="<?=base_url()?>Dashboard/DetailBerita/<?php echo $value->id_berita ?>" style="color: #333333;"><?php echo $value->tanggal;?></td></a>
                  <td><img src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>" alt="" style="width: 80px;height: 80px; object-fit: cover;"></td></a>
            			<?php if ($admin == true) { ?>
            					<td style="text-align: center;"><a href="<?=base_url()?>Dashboard/ViewEditBerita/<?php echo $value->id_berita; ?>"><i class="fa fa-cog" style="font-size: 28px;" aria-hidden="true"></i></td>
            					<td style="text-align: center;"><a href="<?=base_url()?>Dashboard/DeleteBerita/<?php echo $value->id_berita; ?>"><i class="fa fa-trash" style="font-size: 28px;" aria-hidden="true"></i></td>
            			<?php } ?>
            			</tr>
            	<?php }?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?=base_url()?>assets/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>assets/AdminLTE/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url()?>assets/AdminLTE/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable()
    $( ".treeview-tables" ).last().addClass( "active" );
    $( ".menu-berita" ).last().addClass( "active" );
  })
</script>
</body>
</html>
