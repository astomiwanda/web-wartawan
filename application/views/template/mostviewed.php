<?php
$query = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
FROM `berita`
LEFT JOIN `user` AS `user`
ON `berita`.`user_id` = `user`.`id`
LEFT JOIN `kategori` AS `kategori`
ON `berita`.`kategori_id` = `kategori`.`id`
ORDER BY `id_berita` DESC";

$data = $this->db->query($query)->result();
?>

<div class="theiaStickySidebar">
	<div id="posts-list-widget-6" class="container-wrapper widget posts-list">
		<div class="widget-title the-global-title">
			<h4>Most Viewed<span class="widget-title-icon fa"></span></h4>
		</div>
		<div class="posts-list-counter">
			<ul class="posts-list-items">

				<?php $i=0; foreach ($data as $key => $value) { $i++;
				if ($i <= 5) { ?>
				<li class="widget-post-list tie_standard">
					<div class="post-widget-thumbnail">
						<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo word_limiter($value->judul, 6) ?>"
						 class="post-thumb">
							<div class="post-thumb-overlay-wrap">
								<div class="post-thumb-overlay">
									<span class="icon"></span>
								</div>
							</div>
							<img width="220" height="150" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>" class="attachment-jannah-image-small size-jannah-image-small wp-post-image"
							 alt="" />
						</a> </div><!-- post-alignleft /-->
					<div class="post-widget-body">
						<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>"
							 title="<?php echo word_limiter($value->judul, 6) ?>"><?php echo word_limiter($value->judul, 8) ?></a></h3>
						<div class="post-meta">
							<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span>
							<?php echo Beranda::time_elapsed_string(''.$value->tanggal.' '.$value->waktu.'') ?></span></span> </div>
					</div>
				</li>
				<?php }} ?>

			</ul>
		</div>
		<div class="clearfix"></div>
	</div><!-- .widget /-->
