<?php
$last_edit = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
FROM `berita`
LEFT JOIN `user` AS `user`
ON `berita`.`user_id` = `user`.`id`
LEFT JOIN `kategori` AS `kategori`
ON `berita`.`kategori_id` = `kategori`.`id`
ORDER BY `id_berita` DESC";
$data_lastEdit = $this->db->query($last_edit)->result();

?>

<footer id="footer" class="site-footer dark-skin">
	<div id="footer-widgets-container">
		<div class="container">
			<div class="footer-widget-area ">
				<div class="tie-row">
					<div class="tie-col-md-3 normal-side">
						<div id="posts-list-widget-1" class="container-wrapper widget posts-list">
							<div class="widget-title the-global-title">
								<h4>Populer<span class="widget-title-icon fa"></span></h4>
							</div>
							<div class="timeline-widget">
								<ul class="posts-list-items">

									<?php $i=0; foreach ($data_lastEdit as $key => $value) { $i++;
									if ($i <= 3) { ?>
									<li>
										<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>">
											<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span>
											<?php echo $value->tanggal ?></span></span>
											<h3><?php echo $value->judul ?></h3>
										</a>
									</li>
									<?php }} ?>

								</ul>
							</div>
							<div class="clearfix"></div>
						</div><!-- .widget /-->
					</div><!-- .tie-col /-->

					<div class="tie-col-md-3 normal-side">
						<div id="posts-list-widget-2" class="container-wrapper widget posts-list">
							<div class="widget-title the-global-title">
								<h4>Last Modified Posts<span class="widget-title-icon fa"></span></h4>
							</div>
							<div class="posts-pictures-widget">
								<div class="tie-row">

									<?php $i=0; foreach ($data_lastEdit as $key => $value) { $i++;
									if ($i <= 9) { ?>
									<div class="tie-col-xs-4 tie_standard">
										<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo $value->judul ?>"
										 class="post-thumb">
											<div class="post-thumb-overlay-wrap">
												<div class="post-thumb-overlay">
													<span class="icon"></span>
												</div>
											</div>
											<img width="390" height="220" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>" class="attachment-jannah-image-large size-jannah-image-large wp-post-image"
											 alt="" />
										</a>
									</div>
									<?php }} ?>

								</div>
							</div>
							<div class="clearfix"></div>
						</div><!-- .widget /-->
					</div><!-- .tie-col /-->
					<div class="tie-col-md-3 normal-side">
					</div><!-- .tie-col /-->
				</div><!-- .tie-row /-->
			</div><!-- .footer-widget-area /-->

			<div class="footer-widget-area ">
				<div class="tie-row">
					<div class="tie-col-sm-4 normal-side">
						<div id="author-bio-widget-1" class="container-wrapper widget aboutme-widget">
							<div class="about-author about-content-wrapper"><img alt="" src="<?=base_url()?>assets/img/PDAM-DEPOK.jpg" style="margin-top: 15px; margin-bottom: 0px;"
								 class="about-author-img" width="280" height="47">
								<div class="aboutme-widget-content">
								</div>
								<div class="clearfix"></div>
							</div><!-- .about-widget-content -->
							<div class="clearfix"></div>
						</div><!-- .widget /-->
					</div><!-- .tie-col /-->
					<div class="tie-col-sm-4 normal-side">
						<div id="author-bio-widget-2" class="container-wrapper widget aboutme-widget">
							<div class="about-author about-content-wrapper">
								<div class="aboutme-widget-content">mepo.com
								</div>
								<div class="clearfix"></div>
							</div><!-- .about-widget-content -->
							<div class="clearfix"></div>
						</div><!-- .widget /-->
					</div><!-- .tie-col /-->

					<div class="tie-col-sm-4 normal-side">
						<div id="tie-newsletter-1" class="container-wrapper widget subscribe-widget">
							<div id="mc_embed_signup-tie-newsletter-1">
								<form action="#" method="post" id="mc-embedded-subscribe-form-tie-newsletter-1" name="mc-embedded-subscribe-form"
								 class="subscribe-form validate" target="_blank" novalidate>
									<div class="mc-field-group">
										<label class="screen-reader-text" for="mce-EMAIL-tie-newsletter-1">Enter your Email address</label>
										<input type="email" value="" id="mce-EMAIL-tie-newsletter-1" placeholder="Enter your Email address" name="EMAIL"
										 class="subscribe-input required email">
									</div>
									<input type="submit" value="Subscribe" name="subscribe" class="button subscribe-submit">
								</form>
							</div>
							<div class="clearfix"></div>
						</div><!-- .widget /-->
					</div><!-- .tie-col /-->
					<div class=" normal-side">
					</div><!-- .tie-col /-->
				</div><!-- .tie-row /-->
			</div><!-- .footer-widget-area /-->
		</div><!-- .container /-->
	</div><!-- #Footer-widgets-container /-->

	<div id="site-info" class="site-info-layout-2">
		<div class="container">
			<div class="tie-row">
				<div class="tie-col-md-12">
					<div class="copyright-text copyright-text-first">&copy; Copyright 2019, All Rights Reserved</div>
					<div class="footer-menu">
						<ul id="menu-tielabs-secondry-menu" class="menu">
							<li id="menu-item-1014" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1014 tie-current-menu"><a
								 href="<?=base_url()?>Beranda">Home</a></li>
							<li id="menu-item-1015" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1015"><a href="#">Iklan</a></li>
							<li id="menu-item-1254" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1254"><a href="<?=base_url()?>Beranda">Redaksi</a></li>
						</ul>
					</div>
					<ul class="social-icons">
						<li class="social-icons-item"><a class="social-link  facebook-social-icon" title="Facebook" rel="nofollow" target="_blank"
							 href="#"><span class="fa fa-facebook"></span></a></li>
						<li class="social-icons-item"><a class="social-link  twitter-social-icon" title="Twitter" rel="nofollow" target="_blank"
							 href="#"><span class="fa fa-twitter"></span></a></li>
						<li class="social-icons-item"><a class="social-link  youtube-social-icon" title="YouTube" rel="nofollow" target="_blank"
							 href="#"><span class="fa fa-youtube"></span></a></li>
						<li class="social-icons-item"><a class="social-link  instagram-social-icon" title="Instagram" rel="nofollow"
							 target="_blank" href="#"><span class="fa fa-instagram"></span></a></li>
					</ul>
				</div><!-- .tie-col /-->
			</div><!-- .tie-row /-->
		</div><!-- .container /-->
	</div><!-- #site-info /-->
</footer><!-- #footer /-->
