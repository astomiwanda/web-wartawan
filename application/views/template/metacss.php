<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="theme-color" content="#0088ff" />
<style type="text/css">
	img.wp-smiley,
	img.emoji {
		display: inline !important;
		border: none !important;
		box-shadow: none !important;
		height: 1em !important;
		width: 1em !important;
		margin: 0 .07em !important;
		vertical-align: -0.1em !important;
		background: none !important;
		padding: 0 !important;
	}

</style>
<link rel='stylesheet' id='tie-insta-style-css' href='<?=base_url()?>assets/css/style32.css' type='text/css' media='all' />
<link rel='stylesheet' id='taqyeem-buttons-style-css' href='<?=base_url()?>assets/css/style33.css' media='all' />
<link rel='stylesheet' id='sfly-tbgrdr-css-css' href='<?=base_url()?>assets/css/thumbnailgrid34.css' type='text/css'
 media='all' />
<link rel='stylesheet' id='jannah-styles-css' href='<?=base_url()?>assets/css/style35.css' type='text/css' media='all' />
<link rel='stylesheet' id='jannah-ilightbox-skin-css' href='<?=base_url()?>assets/css/skin36.css' type='text/css' media='all' />
<link rel='stylesheet' id='taqyeem-styles-css' href='<?=base_url()?>assets/css/taqyeem37.css' type='text/css' media='all' />
<!-- <link rel="stylesheet" type="text/css" href="assets/css//bootstrap.min.css"> -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fontawesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/slick.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/slick-theme.css">
<style id='taqyeem-styles-inline-css' type='text/css'>
	.wf-active .logo-text,
	.wf-active h1,
	.wf-active h2,
	.wf-active h3,
	.wf-active h4,
	.wf-active h5,
	.wf-active h6 {
		font-family: 'Poppins';
	}

	#main-nav .main-menu>ul>li>a {
		text-transform: uppercase;
	}

	.tie-cat-12,
	.tie-cat-item-12>span {
		background-color: #e67e22 !important;
		color: #FFFFFF !important;
	}

	.tie-cat-12:after {
		border-top-color: #e67e22 !important;
	}

	.tie-cat-12:hover {
		background-color: #c86004 !important;
	}

	.tie-cat-12:hover:after {
		border-top-color: #c86004 !important;
	}

	.tie-cat-19,
	.tie-cat-item-19>span {
		background-color: #2ecc71 !important;
		color: #FFFFFF !important;
	}

	.tie-cat-19:after {
		border-top-color: #2ecc71 !important;
	}

	.tie-cat-19:hover {
		background-color: #10ae53 !important;
	}

	.tie-cat-19:hover:after {
		border-top-color: #10ae53 !important;
	}

	.tie-cat-23,
	.tie-cat-item-23>span {
		background-color: #9b59b6 !important;
		color: #FFFFFF !important;
	}

	.tie-cat-23:after {
		border-top-color: #9b59b6 !important;
	}

	.tie-cat-23:hover {
		background-color: #7d3b98 !important;
	}

	.tie-cat-23:hover:after {
		border-top-color: #7d3b98 !important;
	}

	.tie-cat-24,
	.tie-cat-item-24>span {
		background-color: #34495e !important;
		color: #FFFFFF !important;
	}

	.tie-cat-24:after {
		border-top-color: #34495e !important;
	}

	.tie-cat-24:hover {
		background-color: #162b40 !important;
	}

	.tie-cat-24:hover:after {
		border-top-color: #162b40 !important;
	}

	.tie-cat-28,
	.tie-cat-item-28>span {
		background-color: #795548 !important;
		color: #FFFFFF !important;
	}

	.tie-cat-28:after {
		border-top-color: #795548 !important;
	}

	.tie-cat-28:hover {
		background-color: #5b372a !important;
	}

	.tie-cat-28:hover:after {
		border-top-color: #5b372a !important;
	}

	.tie-cat-29,
	.tie-cat-item-29>span {
		background-color: #4CAF50 !important;
		color: #FFFFFF !important;
	}

	.tie-cat-29:after {
		border-top-color: #4CAF50 !important;
	}

	.tie-cat-29:hover {
		background-color: #2e9132 !important;
	}

	.tie-cat-29:hover:after {
		border-top-color: #2e9132 !important;
	}

	@media (max-width: 991px) {
		.side-aside.dark-skin {
			background: #2f88d6;
			background: -webkit-linear-gradient(135deg, #2f88d6, #5933a2);
			background: -moz-linear-gradient(135deg, #2f88d6, #5933a2);
			background: -o-linear-gradient(135deg, #2f88d6, #5933a2);
			background: linear-gradient(135deg, #2f88d6, #5933a2);
		}
	}

	#mobile-search .search-submit {
		background-color: #0a0000;
		color: #FFFFFF;
	}

	#mobile-search .search-submit:hover {
		background-color: #000000;
	}

</style>
<script type='text/javascript'>
	/* <![CDATA[ */
	var tie = {
		"is_rtl": "",
		"mobile_menu_active": "true",
		"mobile_menu_top": "",
		"mobile_menu_parent": "",
		"lightbox_all": "true",
		"lightbox_gallery": "true",
		"lightbox_skin": "dark",
		"lightbox_thumb": "horizontal",
		"lightbox_arrows": "true",
		"is_singular": "",
		"is_sticky_video": "",
		"reading_indicator": "true",
		"lazyload": "",
		"select_share": "true",
		"select_share_twitter": "true",
		"select_share_facebook": "true",
		"select_share_linkedin": "true",
		"select_share_email": "",
		"facebook_app_id": "",
		"twitter_username": "",
		"responsive_tables": "true",
		"ad_blocker_detector": "",
		"sticky_behavior": "upwards",
		"sticky_desktop": "true",
		"sticky_mobile": "true",
		"is_buddypress_active": "",
		"ajax_loader": "<div class=\"loader-overlay\"><div class=\"spinner-circle\"><\/div><\/div>",
		"type_to_search": "1",
		"lang_no_results": "Nothing Found"
	};
	/* ]]> */

</script>
<script type='text/javascript' src='<?=base_url()?>assets/js/jquery47.js'></script>
<script type='text/javascript' src='<?=base_url()?>assets/js/jquery-migrate48.min.js'></script>
<meta name="generator" content="WordPress 4.9.9" />
<style type="text/css" media="screen"></style>
