<?php
$this->db->where('kategori_id', 1);
$num_terkini = $this->db->get('berita')->num_rows();
$this->db->where('kategori_id', 2);
$num_pemkot = $this->db->get('berita')->num_rows();
$this->db->where('kategori_id', 3);
$num_politik = $this->db->get('berita')->num_rows();
$this->db->where('kategori_id', 4);
$num_pendidikan = $this->db->get('berita')->num_rows();
$this->db->where('kategori_id', 5);
$num_nasional = $this->db->get('berita')->num_rows();
$this->db->where('kategori_id', 6);
$num_inspirasi = $this->db->get('berita')->num_rows();
$this->db->where('kategori_id', 7);
$num_peristiwa = $this->db->get('berita')->num_rows();
$this->db->where('kategori_id', 8);
$num_umkm = $this->db->get('berita')->num_rows();
?>

<div id="tie-widget-categories-1" class="container-wrapper widget widget_categories tie-widget-categories">
	<div class="widget-title the-global-title">
		<h4>Categories<span class="widget-title-icon fa"></span></h4>
	</div>
	<ul>
		<li class="cat-item cat-counter tie-cat-item-4"><a href="<?=base_url()?>terkini/">Terkini</a>
			<span><?php echo $num_terkini ?></span>
		</li>
		<li class="cat-item cat-counter tie-cat-item-34"><a href="<?=base_url()?>pemkot/">Pemkot</a>
			<span><?php echo $num_pemkot ?></span>
		</li>
		<!-- <li class="cat-item cat-counter tie-cat-item-8"><a href="ragam/">Ragam</a>
			<span>310</span>
		</li> -->
		<li class="cat-item cat-counter tie-cat-item-5"><a href="<?=base_url()?>peristiwa/">Peristiwa</a>
			<span><?php echo $num_peristiwa ?></span>
		</li>
		<li class="cat-item cat-counter tie-cat-item-35"><a href="<?=base_url()?>pendidikan/">Pendidikan</a>
			<span>1<?php echo $num_pendidikan ?></span>
		</li>
		<!-- <li class="cat-item cat-counter tie-cat-item-36"><a href="dawah/">Da&#039;wah</a>
			<span>107</span>
		</li> -->
		<!-- <li class="cat-item cat-counter tie-cat-item-38"><a href="tni-polri/">TNI
				&#8211; Polri</a> <span>103</span>
		</li> -->
		<li class="cat-item cat-counter tie-cat-item-11"><a href="<?=base_url()?>politik/">Politik</a>
			<span><?php echo $num_politik ?></span>
		</li>
		<li class="cat-item cat-counter tie-cat-item-6"><a href="<?=base_url()?>inspirasi/">Inspirasi</a>
			<span><?php echo $num_inspirasi ?></span>
		</li>
		<li class="cat-item cat-counter tie-cat-item-37"><a href="<?=base_url()?>nasional/">Nasional</a>
			<span><?php echo $num_nasional ?></span>
		</li>
		<li class="cat-item cat-counter tie-cat-item-7"><a href="<?=base_url()?>umkm/">UMKM</a>
			<span><?php echo $num_umkm ?></span>
		</li>
		<li class="cat-item cat-counter tie-cat-item-9"><a href="<?=base_url()?>berita-foto/">Berita
				Foto</a> <span>5</span>
		</li>
		<!-- <li class="cat-item cat-counter tie-cat-item-1"><a href="http://depokpembaharuan.com/category/uncategorized/">Uncategorized</a>
			<span>1</span>
		</li> -->
	</ul>
	<div class="clearfix"></div>
</div><!-- .widget /-->
</div><!-- .theiaStickySidebar /-->
