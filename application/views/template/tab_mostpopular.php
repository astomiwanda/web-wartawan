<aside class="side-aside normal-side tie-aside-effect dark-skin is-fullwidth" aria-label="Secondary Sidebar">
	<div data-height="100%" class="side-aside-wrapper has-custom-scroll">
		<a href="#" class="close-side-aside remove big-btn light-btn">
			<span class="screen-reader-text">Close</span>
		</a><!-- .close-side-aside /-->

		<div id="mobile-container">
			<div id="mobile-menu" class=""></div><!-- #mobile-menu /-->
			<div class="mobile-social-search">
				<div id="mobile-social-icons" class="social-icons-widget solid-social-icons">
					<ul>
						<li class="social-icons-item"><a class="social-link  facebook-social-icon" title="Facebook" rel="nofollow" target="_blank"
							 href="#"><span class="fa fa-facebook"></span></a></li>
						<li class="social-icons-item"><a class="social-link  twitter-social-icon" title="Twitter" rel="nofollow" target="_blank"
							 href="#"><span class="fa fa-twitter"></span></a></li>
						<li class="social-icons-item"><a class="social-link  youtube-social-icon" title="YouTube" rel="nofollow" target="_blank"
							 href="#"><span class="fa fa-youtube"></span></a></li>
						<li class="social-icons-item"><a class="social-link  instagram-social-icon" title="Instagram" rel="nofollow"
							 target="_blank" href="#"><span class="fa fa-instagram"></span></a></li>
					</ul>
				</div><!-- #mobile-social-icons /-->
				<div id="mobile-search">
					<form role="search" method="get" class="search-form" action="<?=base_url()?>Dashboard/cari">
						<label>
							<span class="screen-reader-text">Search for:</span>
							<input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
						</label>
						<input type="submit" class="search-submit" value="Search" />
					</form>
				</div><!-- #mobile-search /-->
			</div><!-- #mobile-social-search /-->
		</div><!-- #mobile-container /-->

		<div id="slide-sidebar-widgets">
			<div id="posts-list-widget-4" class="container-wrapper widget posts-list">
				<div class="widget-title the-global-title">
					<h4>Popular Posts<span class="widget-title-icon fa"></span></h4>
				</div>
				<div class="posts-list-big-first">
					<ul class="posts-list-items">
						
						<?php if (isset($data_berita[0])) { ?>
						<li class="widget-post-list tie_standard">
							<div class="post-widget-thumbnail">
								<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[0]->id_berita ?>" title="Akibat Menyalip, Siswa Smanti Depok Tergilas Truk Tewas Ditempat"
								 class="post-thumb">
									<h5 class="post-cat-wrap"><span class="post-cat tie-cat-5"><?php echo $data_berita[0]->kategori ?></span></h5>
									<div class="post-thumb-overlay-wrap">
										<div class="post-thumb-overlay">
											<span class="icon"></span>
										</div>
									</div>
									<img width="390" height="220" src="<?=base_url()?>assets/uploads/<?php echo $data_berita[0]->img ?>" class="attachment-jannah-image-large size-jannah-image-large wp-post-image"
									 alt="" sizes="(max-width: 390px) 100vw, 390px" />
								</a> </div><!-- post-alignleft /-->
							<div class="post-widget-body">
								<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[0]->id_berita ?>"
									 title="<?php echo word_limiter($data_berita[0]->judul, 6) ?>"><?php echo word_limiter($data_berita[0]->judul, 8) ?></a></h3>
								<div class="post-meta">
									<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span><?php $date=date_create($value->tanggal); echo date_format($date,"F d, Y"); ?></span></span>
								</div>
							</div>
						</li><!-- first post /-->
						<?php } ?>

						<?php if (isset($data_berita[0])) {
						$i=0; foreach ($data_berita as $key => $value) {
						if ($i <= 4) { ?>
						<li class="widget-post-list tie_standard">
							<div class="post-widget-thumbnail">
								<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[$i]->id_berita ?>" title="<?php echo word_limiter($data_berita[$i]->judul, 6) ?>"
								 class="post-thumb">
									<div class="post-thumb-overlay-wrap">
										<div class="post-thumb-overlay">
											<span class="icon"></span>
										</div>
									</div>
									<img width="220" height="150" src="<?=base_url()?>assets/uploads/<?php echo $data_berita[$i]->img ?>" class="attachment-jannah-image-small size-jannah-image-small wp-post-image"
									 alt="" />
								</a> </div><!-- post-alignleft /-->
							<div class="post-widget-body">
								<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[$i]->id_berita ?>" title="<?php echo word_limiter($data_berita[$i]->judul, 6) ?>"><?php echo word_limiter($data_berita[$i]->judul, 8) ?></a></h3>
								<div class="post-meta">
									<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span>
									<?php $date=date_create($value->tanggal); echo date_format($date,"F d, Y"); ?></span></span>
								</div>
							</div>
						</li><!-- child post -->
						<?php $i++; }}} ?>

					</ul>
				</div>
				<div class="clearfix"></div>
			</div><!-- .widget /-->

			<div id="posts-list-widget-5" class="container-wrapper widget posts-list">
				<div class="widget-title the-global-title">
					<h4>Most Commented<span class="widget-title-icon fa"></span></h4>
				</div>
				<div class="timeline-widget">
					<ul class="posts-list-items">

						<?php if (isset($data_berita[0])) {
						$i=0; foreach ($data_berita as $key => $value) {
						if ($i < 5) { ?>
						<li>
							<a href="<?=base_url()?>NewsDescription/index/<?php echo $data_berita[$i]->id_berita ?>">
								<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span>
								<?php $date=date_create($value->tanggal); echo date_format($date,"F d, Y"); ?></span></span>
								<h3><?php echo word_limiter($value->judul, 8) ?></h3>
							</a>
						</li>
						<?php $i++; }}} ?>

					</ul>
				</div>
				<div class="clearfix"></div>
			</div><!-- .widget /-->

			<div id="comments_avatar-widget-1" class="container-wrapper widget recent-comments-widget">
				<div class="widget-title the-global-title">
					<h4>Recent Comments<span class="widget-title-icon fa"></span></h4>
				</div>
				<ul>
					<?php for ($i=1; $i < 6; $i++) { ?>
					<li>
						<div class="post-widget-thumbnail" style="width:70px">
							<a class="author-avatar" href="#">
							</a>
						</div>
						<div class="comment-body">
							<a class="comment-author" href="#"><?php echo $value->fullname ?></a>
							<p>
								<?php echo $i ?>
							</p>
						</div>
					</li>
					<?php } ?>

				</ul>
				<div class="clearfix"></div>
			</div><!-- .widget /-->
		</div>
	</div><!-- .side-aside-wrapper /-->
</aside><!-- .side-aside /-->
