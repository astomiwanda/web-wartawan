<?php
$query = "SELECT *, `berita`.id AS `id_berita`, `user`.`id` AS `id_user`, `kategori`.`id` AS `id_kategori`
FROM `berita`
LEFT JOIN `user` AS `user`
ON `berita`.`user_id` = `user`.`id`
LEFT JOIN `kategori` AS `kategori`
ON `berita`.`kategori_id` = `kategori`.`id`
ORDER BY `id_berita` DESC";

$data = $this->db->query($query)->result();

?>

<aside class="sidebar tie-col-md-4 tie-col-xs-12 normal-side is-sticky" aria-label="Primary Sidebar">
	<div class="theiaStickySidebar">
		<div id="text-8" class="container-wrapper widget widget_text">
			<div class="textwidget">
				<p><a href="<?=base_url()?>" target="_blank" rel="noopener"><img src="<?=base_url()?>assets/img/PDAM-DEPOK.jpg"
						 alt="Ad Banner 336 x 280" width="336" height="280" /></a></p>
			</div>
			<div class="clearfix"></div>
		</div><!-- .widget /-->
		<div class="container-wrapper tabs-container-wrapper">
			<div class="widget tabs-widget">
				<div class="widget-container">
					<div class="tabs-widget">
						<div class="tabs-wrapper">

							<ul class="tabs-menu">
								<li><a href="#widget_tabs-3-recent">Recent</a></li>
								<li><a href="#widget_tabs-3-popular">Popular</a></li>
								<li><a href="#widget_tabs-3-comments">Comments</a></li>
							</ul><!-- ul.tabs-menu /-->

							<div id="widget_tabs-3-recent" class="tab-content tab-content-recent">
								<ul class="tab-content-elements">

									<?php $i=0; foreach ($data as $key => $value) { $i++;
									if ($i <= 5) { ?>
									<li class="widget-post-list tie_standard">
										<div class="post-widget-thumbnail">
											<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo word_limiter($value->judul, 6) ?>"
											 class="post-thumb">
												<div class="post-thumb-overlay-wrap">
													<div class="post-thumb-overlay">
														<span class="icon"></span>
													</div>
												</div>
												<img width="220" height="150" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>" class="attachment-jannah-image-small size-jannah-image-small wp-post-image"
												 alt="" sizes="(max-width: 220px) 100vw, 220px" />
											</a> </div><!-- post-alignleft /-->
										<div class="post-widget-body">
											<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo word_limiter($value->judul, 6) ?>"><?php echo word_limiter($value->judul, 8) ?></a></h3>
											<div class="post-meta">
												<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span>
														<?php echo $this->M_Berita->time_elapsed_string(''.$value->tanggal.' '.$value->waktu.'') ?></span></span> </div>
										</div>
									</li>
									<?php }} ?>

								</ul>
							</div><!-- .tab-content#recent-posts-tab /-->


							<div id="widget_tabs-3-popular" class="tab-content tab-content-popular">
								<ul class="tab-content-elements">

									<?php $i=0; foreach ($data as $key => $value) { $i++;
									if ($i <= 5) { ?>
									<li class="widget-post-list tie_standard">
										<div class="post-widget-thumbnail">
											<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo word_limiter($value->judul, 6) ?>" class="post-thumb">
												<div class="post-thumb-overlay-wrap">
													<div class="post-thumb-overlay">
														<span class="icon"></span>
													</div>
												</div>
												<img width="200" height="150" src="<?=base_url()?>assets/uploads/<?php echo $value->img ?>" class="attachment-jannah-image-small size-jannah-image-small wp-post-image"
												 alt="" sizes="(max-width: 200px) 100vw, 200px" />
											</a> </div><!-- post-alignleft /-->
										<div class="post-widget-body">
											<h3 class="post-title"><a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo word_limiter($value->judul, 6) ?>"><?php echo word_limiter($value->judul, 8) ?></a></h3>
											<div class="post-meta">
												<span class="date meta-item"><span class="fa fa-clock-o" aria-hidden="true"></span> <span>
												<?php $date=date_create($value->tanggal); echo date_format($date,"F d, Y"); ?></span></span> </div>
										</div>
									</li>
									<?php }} ?>

								</ul>
							</div><!-- .tab-content#popular-posts-tab /-->


							<div id="widget_tabs-3-comments" class="tab-content tab-content-comments">
								<ul class="tab-content-elements">

									<?php for ($i=1; $i < 6; $i++) { ?>
									<li>
										<div class="post-widget-thumbnail" style="width:70px">
											<a class="author-avatar" href="#">
											</a>
										</div>
										<div class="comment-body">
											<a class="comment-author" href="#"><?php echo $value->fullname ?></a>
											<p>
												<?php echo $i ?>
											</p>
										</div>
									</li>
									<?php } ?>

								</ul>
							</div><!-- .tab-content#comments-tab /-->
						</div><!-- .tabs-wrapper-animated /-->
					</div><!-- .tabs-widget /-->
				</div><!-- .widget-container /-->
			</div><!-- .tabs-widget /-->
		</div><!-- .container-wrapper /-->
		<div id="stream-item-widget-6" class="container-wrapper widget stream-item-widget" style="display: none">
			<div class="stream-item-widget-content">
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- Debar 300 x 250 -->
				<ins class="adsbygoogle" style="display:inline-block;width:300px;height:250px" data-ad-client="ca-pub-5782662657508727"
				 data-ad-slot="4006479220"></ins>
				<script>
					(adsbygoogle = window.adsbygoogle || []).push({});

				</script>
			</div>
			<div class="clearfix"></div>
		</div><!-- .widget /-->
	</div><!-- .theiaStickySidebar /-->
</aside><!-- .sidebar /-->
