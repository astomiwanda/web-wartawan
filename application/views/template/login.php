<div id="fb-root"></div>
<div id="tie-popup-login" class="tie-popup">
	<a href="#" class="tie-btn-close remove big-btn light-btn">
		<span class="screen-reader-text">Close</span>
	</a>
	<div class="tie-popup-container">
		<div class="container-wrapper">
			<div class="widget login-widget">
				<div class="widget-title the-global-title">
					<h4>Log In <span class="widget-title-icon fa "></span></h4>
				</div>
				<div class="widget-container">
					<div class="login-form">
						<form action="<?=base_url()?>Login/LoginAction" method="post">
							<input type="text" name="username" title="Username" placeholder="Username">
							<div class="pass-container">
								<input type="password" name="password" title="Password" placeholder="Password">
								<!-- <a class="forget-text" href="http://depokpembaharuan.com/wp-login.php?action=lostpassword&redirect_to=http%3A%2F%2Fdepokpembaharuan.com">Forget?</a> -->
							</div>
							<!-- <input type="hidden" name="redirect_to" value="/" /> -->
							<!-- <label for="rememberme" class="rememberme">
								<input id="rememberme" name="rememberme" type="checkbox" checked="checked" value="forever" /> Remember me
							</label> -->
							<button id="login-submit" type="submit" name="login" value="login" class="button fullwidth">Log In</button>
						</form>
					</div>
				</div><!-- .widget-container  /-->
			</div><!-- .login-widget  /-->
		</div><!-- .container-wrapper  /-->
	</div><!-- .tie-popup-container /-->
</div><!-- .tie-popup /-->
