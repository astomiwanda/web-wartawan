<?php
// $this->db->order_by('id', 'RANDOM');
$this->db->order_by('rand()');
$this->db->limit(1);
$query = $this->db->get('berita');
$data = $query->result_array();
foreach ($data as $key => $value) {
	$id_berita_random = $value['id'];
}

?>

<header id="theme-header" class="header-layout-3 main-nav-dark main-nav-below main-nav-boxed top-nav-active top-nav-light top-nav-above has-shadow has-custom-sticky-logo mobile-header-centered">
	<nav id="top-nav" class="has-date-breaking-components has-breaking-news" aria-label="Secondary Navigation">
		<div class="container">
			<div class="topbar-wrapper d-flex align-items-center">
				<div class="topbar-today-date d-flex align-items-center">
					<i class="fa fa-clock-o" aria-hidden="true"></i>
					<strong class="inner-text">
						<?php echo date('l, F Y') ?></strong>
				</div>
				<div class="tie-alignleft">
					<div class="breaking controls-is-active">
						<span class="breaking-title">
							<span class="fa fa-bolt" aria-hidden="true"></span>
							<span class="breaking-title-text">Trending</span>
						</span>
						<ul id="breaking-news-in-header" class="breaking-news" data-type="reveal" data-arrows="true">

							<?php $i=0; foreach ($data_berita as $key => $value) { $i++;
							if ($i <= 10) { ?>
							<li class="news-item">
								<a href="<?=base_url()?>NewsDescription/index/<?php echo $value->id_berita ?>" title="<?php echo $value->judul ?>">
									<?php echo $value->judul ?></a>
							</li>
							<?php }} ?>

						</ul>
					</div><!-- #breaking /-->
				</div><!-- .tie-alignleft /-->
				<div class="tie-alignright">
					<ul class="components">
						<li class="side-aside-nav-icon menu-item custom-menu-link">
							<a href="#">
								<span class="fa fa-navicon" aria-hidden="true"></span>
								<span class="screen-reader-text">Sidebar</span>
							</a>
						</li>
						<li class="popup-login-icon menu-item custom-menu-link">
							<a href="#" class="lgoin-btn tie-popup-trigger">
								<span class="fa fa-lock" aria-hidden="true"></span>
								<span class="screen-reader-text">Log In</span>
							</a>
						</li>
						<li class="random-post-icon menu-item custom-menu-link">
							<a href="<?=base_url()?>NewsDescription/index/<?php echo $id_berita_random ?>" class="random-post" title="Random Article">
								<span class="fa fa-random" aria-hidden="true"></span>
								<span class="screen-reader-text">Random Article</span>
							</a>
						</li>
						<li class="social-icons-item">
							<a class="social-link  facebook-social-icon" title="Facebook" rel="nofollow" target="_blank" href="#">
								<span class="fa fa-facebook"></span>
							</a>
						</li>
						<li class="social-icons-item">
							<a class="social-link  twitter-social-icon" title="Twitter" rel="nofollow" target="_blank" href="#">
								<span class="fa fa-twitter"></span>
							</a>
						</li>
						<li class="social-icons-item">
							<a class="social-link  youtube-social-icon" title="YouTube" rel="nofollow" target="_blank" href="#">
								<span class="fa fa-youtube"></span>
							</a>
						</li>
						<li class="social-icons-item">
							<a class="social-link  instagram-social-icon" title="Instagram" rel="nofollow" target="_blank" href="#">
								<span class="fa fa-instagram"></span>
							</a>
						</li>
					</ul><!-- Components -->
				</div><!-- .tie-alignright /-->
			</div><!-- .topbar-wrapper /-->
		</div><!-- .container /-->
	</nav><!-- #top-nav /-->

	<div class="container">
		<div class="tie-row logo-row">
			<div class="logo-wrapper">
				<div class="tie-col-md-4 logo-container">
					<a href="#" id="mobile-menu-icon">
						<span class="nav-icon"></span>
					</a>
					<div id="logo">
						<a title="Mepo Pembaharuan" href="<?=base_url()?>">
							<!-- Logo -->
							<img src="<?=base_url()?>assets/img/LOGO1.png" alt="Depok Pembaharuan" class="logo_normal" width="1280" height="709"
							 style="max-height: 80px; width: auto;">
							<img src="<?=base_url()?>assets/img/LOGO1.png" alt="Depok Pembaharuan" class="logo_2x" width="1280" height="709"
							 style="max-height: 80px; width: auto;">
						</a>
					</div><!-- #logo /-->
				</div><!-- .tie-col /-->
			</div><!-- .logo-wrapper /-->
			<div class="tie-col-md-8 stream-item stream-item-top-wrapper">
				<div class="stream-item-top">
					<a href="#" title="Iklan" target="_blank" rel="nofollow">
						<!-- Iklan -->
						<!-- <img src="assets/img/LOGO1.png" alt="Buy Jannah Theme" width="728" height="90" /> -->
					</a>
				</div>
			</div><!-- .tie-col /-->
		</div><!-- .tie-row /-->
	</div><!-- .container /-->
	<div class="main-nav-wrapper">
		<nav id="main-nav" data-skin="search-in-main-nav live-search-dark" class=" live-search-parent" aria-label="Primary Navigation">
			<div class="container">
				<div class="main-menu-wrapper">
					<div id="menu-components-wrap">
						<div id="sticky-logo">
							<a title="Mepo Pembaharuan" href="<?=base_url()?>">
								<!-- Logo Navbar -->
								<img src="<?=base_url()?>assets/img/LOGO2.png" alt="Depok Pembaharuan" class="logo_normal" style="max-height:49px; width: auto;">
								<img src="<?=base_url()?>assets/img/LOGO2.png" alt="Depok Pembaharuan" class="logo_2x" style="max-height:49px; width: auto;">
							</a>
						</div><!-- #Sticky-logo /-->
						<div class="flex-placeholder"></div>
						<div class="main-menu main-menu-wrap tie-alignleft">
							<div id="main-nav-menu" class="main-menu">
								<ul id="menu-debar" class="menu" role="menubar">
									<li id="menu-beranda" class="menu-beranda menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-1087 current-post-ancestor current-menu-parent current-post-parent menu-item-246"><a
										 href="<?=base_url()?>Beranda">Beranda</a></li>
									<li id="menu-terkini" class="menu-terkini menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1087"><a
										 href="<?=base_url()?>Terkini">Terkini</a></li>
									<li id="menu-pemkot" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1088"><a
										 href="<?=base_url()?>Pemkot">Pemkot</a></li>
									<li id="menu-politik" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1089"><a
										 href="<?=base_url()?>Politik">Politik</a></li>
									<li id="menu-pendidikan" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1090"><a
										 href="<?=base_url()?>Pendidikan">Pendidikan</a></li>
									<li id="menu-nasional" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1255"><a
										 href="<?=base_url()?>Nasional">Nasional</a></li>
									<li id="menu-inspirasi" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-247"><a
										 href="<?=base_url()?>Inspirasi">Inspirasi</a></li>
									<li id="menu-peristiwa" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-249"><a
										 href="<?=base_url()?>Peristiwa">Peristiwa</a></li>
									<li id="menu-umkm" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-250"><a href="<?=base_url()?>UMKM">UMKM</a></li>
									<li id="menu-berita-foto" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-251"><a
										 href="<?=base_url()?>BeritaFoto">Berita Foto</a></li>
									<?php if ($this->session->userdata('status') == 'login') { ?>
									<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-248">
										<a href="<?=base_url()?>Dashboard" >Login</a></li>
									<?php }else{ ?>
									<li class="popup-login-icon menu-item custom-menu-link">
										<a href="#" class="lgoin-btn tie-popup-trigger">Login</a></li>
									<?php } ?>
								</ul>
							</div>
						</div><!-- .main-menu.tie-alignleft /-->
						<ul class="components">
							<li class="search-bar menu-item custom-menu-link" aria-label="Search">
								<form method="get" id="search" action="<?=base_url()?>">
									<input id="search-input" class="is-ajax-search" type="text" name="s" title="Search for" placeholder="Search for" />
									<button id="search-submit" type="submit"><span class="fa fa-search" aria-hidden="true"></span></button>
								</form>
							</li>
							<li class="random-post-icon menu-item custom-menu-link">
								<a href="<?=base_url()?>NewsDescription/index/<?php echo $id_berita_random ?>" class="random-post" title="Random Article">
									<span class="fa fa-random" aria-hidden="true"></span>
									<span class="screen-reader-text">Random Article</span>
								</a>
							</li>
						</ul><!-- Components -->
					</div><!-- #menu-components-wrap /-->
				</div><!-- .main-menu-wrapper /-->
			</div>
			<!-- .container /--
		</nav><!-- #main-nav /-->
	</div><!-- .main-nav-wrapper /-->
</header>
