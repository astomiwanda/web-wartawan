<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- <div class="user-panel">
        <div class="pull-left image">
          <img src="assets/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <i class="fa fa-circle text-success"></i> Online
        </div>
      </div> -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview treeview-dashboard" style="display: none;">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="DetailTransaksi"><a href="<?=base_url()?>Transaksi/DetailTransaksi"><i class="fa fa-circle-o"></i> Detail Transaksi</a></li>
            <li class="LaporanTransaksi"><a href="<?=base_url()?>Transaksi/LaporanTransaksi"><i class="fa fa-circle-o"></i> Laporan Transaksi</a></li>
            <li class="LaporanPembelian"><a href="<?=base_url()?>Transaksi/LaporanPembelian"><i class="fa fa-circle-o"></i> Laporan Pembelian</a></li>
          </ul>
        </li>
        <li class="treeview treeview-charts" style="display: none;">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Charts</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li class="menu-charts"><a href="<?=base_url()?>Chart"><i class="fa fa-circle-o"></i> Transaksi</a></li>
          </ul>
        </li>
        <li class="treeview treeview-tables">
          <a href="#">
            <i class="fa fa-table"></i> <span>Berita</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li class="menu-berita"><a href="<?=base_url()?>Dashboard"><i class="fa fa-circle-o"></i> Berita</a></li>
            <li class="menu-kategori"><a href="<?=base_url()?>Dashboard/Kategori"><i class="fa fa-circle-o"></i> Kategori</a></li>
            <?php foreach ($user as $value) {
              if ($value->level == 'Admin') { ?>
            		<li class="menu-user"><a href="<?=base_url()?>Dashboard/user"><i class="fa fa-circle-o"></i> User</a></li>
        	  <?php }} ?>
            <li class="menu-logout"><a href="<?=base_url()?>Login/logout"><i class="fa fa-circle-o"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>