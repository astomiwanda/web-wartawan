<?php require_once 'dashboard/top_navbar.php' ?>
<?php require_once 'dashboard/left_navbar.php' ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Mepo Metropolitan
			<small>Tambah Informasi Berita</small>
		</h1>
		<!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol> -->
	</section>

	<!-- Main content -->
	<section>
		<div class="content-header">
			<div class="row" style="margin-top: 50px;display: flex;justify-content: center;">
				<div class="col-sm-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">Berita</h3>
						</div>
						<!-- /.box-header -->
						<!-- form start -->
						<form id="tambah_barang" action="<?=base_url()?>Dashboard/AddActionBerita" method="post" class="form-horizontal"
						 enctype="multipart/form-data">
							<div class="box-body">
								<div class="form-group" style="margin-bottom: 0px;">
									<div class="col-sm-4">
										<label for="judul">Lokasi</label>
										<input class="form-control" type="text" name="lokasi" id="">
									</div>
									<div class="col-sm-3">
										<label for="judul">Tema</label>
										<input class="form-control" type="text" name="tema" id="">
									</div>
									<div class="col-sm-2">
										<label for="judul">Kategori</label>
										<select name="kategori" class="form-control">
											<?php foreach ($this->M_Berita->get('kategori') as $key => $value) { ?>
											<option value="<?php echo $value->id ?>">
												<?php echo $value->kategori ?>
											</option>
											<?php } ?>
										</select>
									</div>
									<div class="col-sm-3">
										<label for="exampleInputFile">File input</label>
										<input type="file" name="berkas" id="exampleInputFile" class="form-control">
										<p class="help-block">Max 2MB.</p>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<label for="judul">Judul</label>
										<input class="form-control" type="text" name="judul" id="">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<label for="inputEmail3" class="control-label">Deskripsi isi Berita</label>
										<textarea name="isi" id="isi" class="form-control" cols="30" rows="10" required></textarea>
									</div>
								</div>
							</div>
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="button" onclick="window.location.href='<?=base_url()?>Dashboard'" class="btn btn-default">Cancel</button>
								<button type="submit" name="tambah_berita" value="tambah_berita" class="btn btn-info pull-right">Tambah</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>
					<!-- /.box -->
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php require_once 'dashboard/footer.php' ?>
<script type="text/javascript">
	$(".treeview-tables").last().addClass("active");
	$(".menu-berita").last().addClass("active");

</script>
